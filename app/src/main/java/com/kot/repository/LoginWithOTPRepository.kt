package com.kot.repository

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.kot.api.ApiInterface
import com.kot.model.ErrorStatusReply
import com.kot.model.Event
import com.kot.model.requestmodels.LoginWithMobileRequest
import com.kot.model.requestmodels.LoginWithOTPRequest
import com.kot.model.responsemodels.MemberData
import com.kot.model.responsemodels.SuccessStatusReply
import com.kot.util.SuperRepository
import com.kot.util.SuperRepositoryCallback

class LoginWithOTPRepository(private val apiInterface: ApiInterface, context: Application):SuperRepository(context) {

    val mutableLiveDataLoginOTPResult : MutableLiveData<Event<MemberData>> = MutableLiveData()
    val mutableLiveDataLoginOTPError: MutableLiveData<Event<ErrorStatusReply>> = MutableLiveData()

    val mutableLiveDataLoginResult : MutableLiveData<Event<SuccessStatusReply>> = MutableLiveData()
    val mutableLiveDataLoginError: MutableLiveData<Event<ErrorStatusReply>> = MutableLiveData()
    fun loginWithOTPMobile(loginWithEmailRequest: LoginWithOTPRequest){
        makeApiCall(
            observable = apiInterface.mobileOTPGenerate(loginWithEmailRequest),
            responseJsonKeyword = "member",
            successMutableLiveData = mutableLiveDataLoginOTPResult,
            errorMutableLiveData = mutableLiveDataLoginOTPError,
            callback = object : SuperRepositoryCallback<MemberData>{
                override fun success(result: MemberData) {
                   saveUserDataToPref(result)
                }
                override fun error(statusReply: ErrorStatusReply) {

                }
            }
        )
    }

    fun loginWithMobile(loginWithEmailRequest: LoginWithMobileRequest){
        makeApiCall(
            observable = apiInterface.mobileOTPGenerate(loginWithEmailRequest),
            responseJsonKeyword = "",
            successMutableLiveData = mutableLiveDataLoginResult,
            errorMutableLiveData = mutableLiveDataLoginError,
            callback = object : SuperRepositoryCallback<SuccessStatusReply>{
                override fun success(result: SuccessStatusReply) {
                    // saveUserDataToPref(result)
                }
                override fun error(statusReply: ErrorStatusReply) {

                }
            }
        )
    }

    val mutableLiveDataOTPResult : MutableLiveData<Event<String>> = MutableLiveData()
    val mutableLiveDataOTPError: MutableLiveData<Event<ErrorStatusReply>> = MutableLiveData()
    fun OTPGeneration(loginWithEmailRequest: LoginWithMobileRequest){
        makeApiCall<String>(
            observable = apiInterface.OTPGenerate(loginWithEmailRequest),
            responseJsonKeyword = "otp",
            isResponseAString = true,
            successMutableLiveData = mutableLiveDataOTPResult,
            errorMutableLiveData = mutableLiveDataOTPError,
            callback = object : SuperRepositoryCallback<String> {
                override fun success(result: String) {
                    // saveUserDataToPref(result)
                }
                override fun error(statusReply: ErrorStatusReply) {

                }
            }
        )
    }

}