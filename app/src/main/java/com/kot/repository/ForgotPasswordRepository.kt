package com.kot.repository

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.kot.api.ApiInterface
import com.kot.model.ErrorStatusReply
import com.kot.model.Event
import com.kot.model.requestmodels.ForgotPasswordRequest
import com.kot.util.SuperRepository

class ForgotPasswordRepository(private val apiInterface: ApiInterface, context: Application):SuperRepository(context) {

    val mutableLiveDataforgotPasswordResult : MutableLiveData<Event<ErrorStatusReply>> = MutableLiveData()
    val mutableLiveDataforgotPasswordError: MutableLiveData<Event<ErrorStatusReply>> = MutableLiveData()
    fun forgotPassword(forgotPasswordRequest: ForgotPasswordRequest){
        makeApiCall(
            observable = apiInterface.forgotPassword(forgotPasswordRequest),
            successMutableLiveData = mutableLiveDataforgotPasswordResult,
            errorMutableLiveData = mutableLiveDataforgotPasswordError
        )
    }







}