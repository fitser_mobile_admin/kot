package com.kot.repository

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.kot.api.ApiInterface
import com.kot.model.ErrorStatusReply
import com.kot.model.Event

import com.kot.model.requestmodels.LoginWithEmailRequest
import com.kot.model.responsemodels.MemberData
import com.kot.util.SuperRepository
import com.kot.util.SuperRepositoryCallback

class LoginWithEmailRepository(private val apiInterface: ApiInterface, context: Application):SuperRepository(context) {

    val mutableLiveDataLoginResult : MutableLiveData<Event<MemberData>> = MutableLiveData()
    val mutableLiveDataLoginError: MutableLiveData<Event<ErrorStatusReply>> = MutableLiveData()
    fun loginWithEmail(loginWithEmailRequest: LoginWithEmailRequest){
        makeApiCall(
            observable = apiInterface.loginWithEmail(loginWithEmailRequest),
            responseJsonKeyword = "member",
            successMutableLiveData = mutableLiveDataLoginResult,
            errorMutableLiveData = mutableLiveDataLoginError,
            callback = object : SuperRepositoryCallback<MemberData>{
                override fun success(result: MemberData) {
                    saveUserDataToPref(result)
                }
                override fun error(statusReply: ErrorStatusReply) {

                }
            }
        )
    }







}