package com.kot.repository

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.kot.model.Event
import com.kot.api.ApiInterface
import com.kot.model.ErrorStatusReply
import com.kot.model.requestmodels.MyOrderRequest
import com.kot.model.responsemodels.MyOrderResponceData
import com.kot.util.SuperRepository

class FoodMyOrderRepository(private val apiInterface: ApiInterface, context: Application):
    SuperRepository(context) {

    val mutableLiveDataMyOrderDataResult : MutableLiveData<Event<ArrayList<MyOrderResponceData>>> = MutableLiveData()
    val mutableLiveDataMyOrderDataError: MutableLiveData<Event<ErrorStatusReply>> = MutableLiveData()


    fun getMYOrder(myOrderRequest: MyOrderRequest){
        makeApiCall(
            observable = apiInterface.getMyOrderRequest(myOrderRequest),
            responseJsonKeyword = "data",
            successMutableLiveData = mutableLiveDataMyOrderDataResult,
            errorMutableLiveData = mutableLiveDataMyOrderDataError)
    }

}