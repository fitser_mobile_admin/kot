package com.kot.repository

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.kot.api.ApiInterface
import com.kot.model.ErrorStatusReply
import com.kot.model.Event
import com.kot.model.requestmodels.LoginWithMobileRequest
import com.kot.model.responsemodels.SuccessStatusReply
import com.kot.util.SuperRepository
import com.kot.util.SuperRepositoryCallback

class LoginWithMobileRepository(private val apiInterface: ApiInterface, context: Application):SuperRepository(context) {

    val mutableLiveDataLoginResult : MutableLiveData<Event<SuccessStatusReply>> = MutableLiveData()
    val mutableLiveDataLoginError: MutableLiveData<Event<ErrorStatusReply>> = MutableLiveData()
    fun loginWithMobile(loginWithEmailRequest: LoginWithMobileRequest){
        makeApiCall(
            observable = apiInterface.mobileOTPGenerate(loginWithEmailRequest),
            responseJsonKeyword = "",
            successMutableLiveData = mutableLiveDataLoginResult,
            errorMutableLiveData = mutableLiveDataLoginError,
            callback = object : SuperRepositoryCallback<SuccessStatusReply>{
                override fun success(result: SuccessStatusReply) {
                   // saveUserDataToPref(result)
                }
                override fun error(statusReply: ErrorStatusReply) {

                }
            }
        )
    }

}