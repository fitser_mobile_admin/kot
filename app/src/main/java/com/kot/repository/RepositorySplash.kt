package com.kot.repository

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.kot.BuildConfig
import com.kot.api.ApiInterface
import com.kot.core.KOTApplication
import com.kot.model.ErrorStatusReply
import com.kot.model.requestmodels.CheckUpdateRequest
import com.kot.model.Event
import  com.kot.model.responsemodels.UpdateAppResponse
import com.kot.util.SuperRepository

class RepositorySplash(
    private val apiInterface: ApiInterface,
    context: KOTApplication
) :
    SuperRepository(context) {

    var mutableLiveDataUpdateResponse: MutableLiveData<Event<UpdateAppResponse>> = MutableLiveData()
    var mutableLiveDataUpdateResponseError: MutableLiveData<Event<ErrorStatusReply>> = MutableLiveData()
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //apiInterface calls

    fun checkForUpdate() {
        makeApiCall(
            apiInterface.checkIfUpdateRequired(
                CheckUpdateRequest(
                    (
                            BuildConfig.VERSION_CODE
                            ).toString()
                )
            ),
            responseJsonKeyword = "updateResponse",
            successMutableLiveData = mutableLiveDataUpdateResponse,
            errorMutableLiveData = mutableLiveDataUpdateResponseError
        )
    }
}
