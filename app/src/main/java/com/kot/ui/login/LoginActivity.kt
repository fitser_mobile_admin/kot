package com.kot.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.kot.R
import com.kot.core.KOTApplication
import com.kot.databinding.ActivityLoginBinding
import com.kot.model.ErrorStatusReply
import com.kot.ui.dashboard.DashboardActivity
import com.kot.util.AndroidUtility
import com.kot.util.SuperActivity
import com.google.firebase.analytics.FirebaseAnalytics
import com.kot.ui.signupwithphone.SignupWithPhoneActivity


class LoginActivity:SuperActivity() {

    private lateinit var binding:ActivityLoginBinding

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private var facebookId: String? = ""
//    private  val viewModel : LoginWithEmailViewModel by lazy{ ViewModelProvider(this, viewModelProvider).get(
//        LoginWithEmailViewModel :: class.java)}
//    private  val viewModelProvider: LoginWithEmailViewModelProvider by lazy{ LoginWithEmailViewModelProvider((this.applicationContext as KOTApplication).loginWithEmailRepository, (this.applicationContext as KOTApplication)) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_login)
        binding.context = this
        binding.topView.actionBar.toolbarTitle.visibility=View.GONE
        binding.topView.actionBar.ivBack.visibility = View.GONE
        initView()
        //printHashKey(this)

    }

    override fun onResume() {
        super.onResume()
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        firebaseAnalytics.setCurrentScreen(this, "Login with phone,email,facebook", null /* class override */)
    }

    private fun initView(){
        binding.rlPhoneNumber.setOnClickListener {
           movetoSignupPhone()


        }
        binding.rlEmail.setOnClickListener {
            movetoLoginMail()
        }

       // initViewModelObserver()
    }

    private fun movetoSignupPhone() {
        val intent = Intent(this, SignupWithPhoneActivity::class.java)
        startActivity(intent)
        overridePendingTransition(0,0)
    }


    private fun movetoLoginMail(){
        val intent = Intent(this, LoginWithEmailActivity::class.java)
        startActivity(intent)
        overridePendingTransition(0,0)
    }




    /*private fun initViewModelObserver(){
        viewModel.mediatorLiveDataLoginWithEmailResult.observe(
            this,
            Observer { t ->
                t?.let{
                    if (it.shouldReadContent()){
                        it.readContent()
                        hideProgress()
                        moveToDashboard()
                    }
                }
            })

        viewModel.mediatorLiveDataLoginWithEmailError.observe(
            this,
            Observer { t ->
                t?.let{ if (it.shouldReadContent())
                    loginError(it.getContent()?: ErrorStatusReply(1,getString(com.kot.R.string.something_went_wrong),null))
                }
            }
        )
    }*/

    private fun moveToDashboard(){
        val intent = Intent(this, DashboardActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)

    }
    private fun loginError(errorStatusReply: ErrorStatusReply) {
        hideProgress()
        AndroidUtility.showSnackBar(this, errorStatusReply.message)
    }



    override fun onBackPressed() {
        super.onBackPressed()
       /* val intent = Intent(this, DashboardActivity::class.java)
        startActivity(intent)
        finish()*/
    }

}