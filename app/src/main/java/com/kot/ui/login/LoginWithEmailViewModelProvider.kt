package com.kot.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kot.core.KOTApplication
import com.kot.repository.LoginWithEmailRepository

class LoginWithEmailViewModelProvider(private val repository : LoginWithEmailRepository, private val application: KOTApplication) : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return LoginWithEmailViewModel(repository,application) as T
    }

}