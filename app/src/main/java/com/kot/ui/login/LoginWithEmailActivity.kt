package com.kot.ui.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.kot.R
import com.kot.core.DEVICE_TOKEN
import com.kot.core.KOTApplication
import com.kot.databinding.ActivityLoginWithMailBinding
import com.kot.model.ErrorStatusReply
import com.kot.model.requestmodels.LoginWithEmailRequest
import com.kot.ui.dashboard.DashboardActivity
import com.kot.ui.forgotpassword.ForgotPasswordActivity
import com.kot.util.AndroidUtility
import com.kot.util.Prefs
import com.kot.util.SuperActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult

class LoginWithEmailActivity:SuperActivity() {

    private lateinit var firebaseAnalytics:FirebaseAnalytics
    private lateinit var binding : ActivityLoginWithMailBinding
    //private  val viewModel : LoginWithEmailViewModel by lazy{ ViewModelProvider(this, viewModelProvider).get(LoginWithEmailViewModel :: class.java)}
   // private  val viewModelProvider: LoginWithEmailViewModelProvider by lazy{ LoginWithEmailViewModelProvider((this.applicationContext as KOTApplication).loginWithEmailRepository, (this.applicationContext as KOTApplication)) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_login_with_mail)
        binding.context = this
        intiView()

    }

    override fun onResume() {
        super.onResume()
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        firebaseAnalytics.setCurrentScreen(this, "Login with email", null /* class override */)
    }

    private fun intiView(){
        binding.actionBar.ivBack
        .setOnClickListener {
            onBackPressed()
        }
        binding.tvSubmit.setOnClickListener {
             moveToDashboard()
            //callLoginApi()
          //  generateDeviceToken()

        }

   //     initViewModelObserver()

    }


    fun clickedToForgotPassword(){
        val intent = Intent(this, ForgotPasswordActivity::class.java)
        startActivity(intent)
        overridePendingTransition(0,0)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        overridePendingTransition(0,0)
    }

    private fun moveToDashboard(){
        val intent = Intent(this, DashboardActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)

    }





    /*private fun initViewModelObserver(){
        viewModel.mediatorLiveDataLoginWithEmailResult.observe(
            this,
            Observer { t ->
                t?.let{
                    if (it.shouldReadContent()){
                        it.readContent()
                        hideProgress()
                        moveToDashboard()
                    }
                }
            })

        viewModel.mediatorLiveDataLoginWithEmailError.observe(
            this,
            Observer { t ->
                t?.let{ if (it.shouldReadContent())
                    loginError(it.getContent()?: ErrorStatusReply(1,getString(R.string.something_went_wrong),null))
                }
            }
        )
    }*/

    private fun loginError(errorStatusReply: ErrorStatusReply) {
        hideProgress()
        AndroidUtility.showSnackBar(this, errorStatusReply.message)
    }

    private fun callLoginApi(){
        if(!AndroidUtility.isNetworkAvailable(this)){
            AndroidUtility.showSnackBar(this,getString(R.string.pls_check_internet_con))
            return
        }
        val email = binding.edtEmail.text.toString().trim()
        val password = binding.edtPassword.text.toString().trim()
        when{
            email==""->{
                AndroidUtility.showSnackBar(this,"Email can't be blank")
                return
            }
            (!AndroidUtility.isValidEmail(email)) -> {
                AndroidUtility.showSnackBar(this, "Please enter a valid email.")
                return
            }

            password==""->{
                AndroidUtility.showSnackBar(this,"Password can't be blank")
                return
            }
        }
        showProgress()
        val loginWithEmailRequest = LoginWithEmailRequest()
        loginWithEmailRequest.email = email
        loginWithEmailRequest.password = password
        loginWithEmailRequest.device_token= Prefs.with(this@LoginWithEmailActivity).read(DEVICE_TOKEN,"")
        loginWithEmailRequest.device_type="2"
        //viewModel.loginWithEmail(loginWithEmailRequest)
    }

   private fun generateDeviceToken(){

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(object : OnCompleteListener<InstanceIdResult> {
                override fun onComplete(task: Task<InstanceIdResult>) {
                    if (!task.isSuccessful) {
                        return
                    }
                    val token = task.result?.token?:""
                    Log.v("TOKEN:- ",token)
                    Prefs.with(this@LoginWithEmailActivity).write(DEVICE_TOKEN,token)
                    callLoginApi()
                }
            })
    }
}