package com.kot.ui.login

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MediatorLiveData
import com.kot.core.KOTApplication
import com.kot.model.ErrorStatusReply
import com.kot.model.Event
import com.kot.model.requestmodels.LoginWithEmailRequest
import com.kot.model.responsemodels.MemberData
import com.kot.repository.LoginWithEmailRepository

class LoginWithEmailViewModel(private val repository: LoginWithEmailRepository, val application: KOTApplication) : AndroidViewModel(application) {

     val mediatorLiveDataLoginWithEmailResult : MediatorLiveData<Event<MemberData>> = MediatorLiveData()
     val mediatorLiveDataLoginWithEmailError : MediatorLiveData<Event<ErrorStatusReply>> = MediatorLiveData()


    init{
        bindToRepo()
    }

    private fun bindToRepo(){
        mediatorLiveDataLoginWithEmailResult.addSource(repository.mutableLiveDataLoginResult) {t -> mediatorLiveDataLoginWithEmailResult.postValue(t)}
        mediatorLiveDataLoginWithEmailError.addSource(repository.mutableLiveDataLoginError) {t -> mediatorLiveDataLoginWithEmailError.postValue(t)}
    }

    fun loginWithEmail(loginWithEmailRequest: LoginWithEmailRequest){
        repository.loginWithEmail(loginWithEmailRequest)
    }


}