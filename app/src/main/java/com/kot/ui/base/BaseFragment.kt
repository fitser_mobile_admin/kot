 package com.kot.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment


 abstract class BaseFragment(val title:String) : Fragment() {

    protected lateinit var contentView: View
    protected lateinit var base_activity : BaseActivity
    protected abstract fun getLayout(container: ViewGroup,inflater: LayoutInflater): View
    protected abstract fun afterActivityCreated(savedInstanceState: Bundle?)
    protected abstract fun afterFragmentResume()
     

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (container != null)
            contentView = getLayout(container,inflater).rootView
        return contentView
    }

    protected fun inflateLayout(container: ViewGroup, layout: Int): View {
        val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(layout, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        base_activity = activity as BaseActivity
        base_activity.setTv_title(title)
        afterActivityCreated(savedInstanceState)


    }

    override fun onResume() {
        super.onResume()
        afterFragmentResume()
    }

//    private fun setupBackPressedListener() {
//        contentView.isFocusableInTouchMode = true
//        contentView.requestFocus()
//        contentView.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
//            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
//                base_activity.doBack()
//                return@OnKeyListener true
//            }
//            false
//        })
//    }


}