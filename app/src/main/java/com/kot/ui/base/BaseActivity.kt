package com.kot.ui.base

import android.Manifest
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.kot.R
import com.kot.core.KOTApplication
import com.kot.model.responsemodels.MemberData
import com.kot.ui.dialog.CustomLoaderDialog
import com.kot.ui.dialog.CustomYesNoDialog
import com.kot.ui.interfaces.OnYesNoClickListener
import com.kot.ui.login.LoginActivity
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions


abstract class BaseActivity : AppCompatActivity() {

    companion object {
        val container = R.id.container;
    }

    lateinit var mCustomLoaderDialog: CustomLoaderDialog
    private var btn_menu: ImageView? = null
    private var btn_back: ImageView? = null
    private var tv_title: TextView? = null


    private var navigation_drawer: DrawerLayout? = null
    private var actionBarDrawerToggle: ActionBarDrawerToggle? = null
    private var tool_bar: Toolbar? = null
    private val storagePermissions = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    protected abstract val menuId: Int

    protected abstract val backBtnId: Int

    protected abstract val tvTitleId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setNavigationTheme()
    }

    private fun setNavigationTheme() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.navigationBarColor = resources.getColor(R.color.colorPrimaryDark)
        }
        mCustomLoaderDialog = CustomLoaderDialog(this@BaseActivity)
    }


    fun setTv_title(title: String) {
        this.tv_title?.text = title
    }

    protected fun setToolBar() {
       /* this.navigation_drawer = drawerLayout
        actionBarDrawerToggle =
            ActionBarDrawerToggle(this, navigation_drawer, R.string.app_name, R.string.app_name)
        actionBarDrawerToggle?.syncState()
        navigation_drawer?.addDrawerListener(actionBarDrawerToggle!!)*/
        initHeader(menuId, backBtnId, tvTitleId)
       initDrawerMenuView()

    }


    fun hideToolbar() {
        tool_bar?.visibility = View.GONE
    }

    fun showToolBar() {
        tool_bar?.visibility = View.VISIBLE
    }

    protected abstract fun initDrawerMenuView()

    private fun initHeader(menu_id: Int, back_btn_id: Int, title_txt_id: Int) {
        btn_menu = findViewById<View>(menu_id) as ImageView
        btn_back = findViewById<View>(back_btn_id) as ImageView
        tv_title = findViewById<View>(title_txt_id) as TextView

        if (btn_menu != null) {
            btn_menu?.setOnClickListener {// navigation_drawer?.openDrawer(Gravity.LEFT)
            }
        }

        if (btn_back != null) {
            btn_back?.setOnClickListener { doBack() }
        }

    }

    fun closeNavDrawer() {
       // navigation_drawer!!.openDrawer(Gravity.LEFT)
    }

    private val TIME_INTERVAL =
        2000 // # milliseconds, desired time passed between two back presses.
    private var mBackPressed: Long = 0

    fun doBack() {
        //manageOrientation()



            val fragmentManager = supportFragmentManager
            var count = fragmentManager.backStackEntryCount
            if (count == 1) {
                /*//finish()
                if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                    finish()
                } else {
                    AndroidUtility.showToast(this, getString(R.string.press_back_again))
                    mBackPressed = System.currentTimeMillis()
                }*/

                val mCustomYesNoDialog = CustomYesNoDialog(this,
                    getString(R.string.app_name), "Are you sure, you want exit application?",
                    "Yes", "No", object : OnYesNoClickListener {
                        override fun onYesClicked() {
                            finish()
                        }

                        override fun onNoClicked() {

                        }
                    })
                mCustomYesNoDialog.show()
            } else {
                try {
                    fragmentManager.popBackStackImmediate()
                    count = fragmentManager.backStackEntryCount
                    if (count == 1) {
                        hideBackButton()
                    }
                }catch (ex:Exception){
                    ex.printStackTrace()
                }

            }

    }


    fun startFragment(fragment_container: Int, fragment: Fragment, clearBackStack: Boolean) {
        doStartFragment(fragment_container, fragment, clearBackStack)
    }


    fun startFragment(fragment_container: Int, fragment: Fragment) {
        doStartFragment(fragment_container, fragment, false)
    }

    private fun doStartFragment(
        fragment_container: Int,
        fragment: Fragment, clearBackStack: Boolean
    ) {

        if (navigation_drawer != null && navigation_drawer!!.isDrawerOpen(Gravity.LEFT)) {
            navigation_drawer!!.closeDrawer(Gravity.LEFT)
        }


        if (clearBackStack) {
            clearBackStack()
        }

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

        //setCustomAnimations(int enter, int exit, int popEnter, int popExit)
        fragmentTransaction.setCustomAnimations(
            android.R.anim.slide_in_left,
            android.R.anim.slide_out_right,
            android.R.anim.slide_in_left,
            android.R.anim.slide_out_right
        )

        fragmentTransaction.replace(fragment_container, fragment)
        fragmentTransaction.addToBackStack(fragment.javaClass.name)
        Log.d("BaeActivity:", "Name: " + fragment.javaClass.name)
        fragmentTransaction.commit()
        fragmentManager.executePendingTransactions()
        autoShowHideBack()

    }

    fun showLoader() {
        mCustomLoaderDialog.show()
    }

    fun hideLoader() {
        if (mCustomLoaderDialog.isShowing)
            mCustomLoaderDialog.cancel()
    }

    fun showBackButton() {
        showHideBack(true)
    }

    fun hideBackButton() {
        showHideBack(false)
    }


    private fun autoShowHideBack() {
        val manager = supportFragmentManager
        val count = manager.backStackEntryCount
        Log.d("BaeActivity:", "count: $count")
        val isShow = count > 1
        showHideBack(isShow)
    }

    fun showHideBack(isShowBack: Boolean) {
        if (isShowBack) {
            btn_back?.visibility = View.VISIBLE
            btn_menu?.visibility = View.INVISIBLE
            navigation_drawer!!.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        } else {
            navigation_drawer?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            btn_back?.visibility = View.GONE
            btn_menu?.visibility = View.VISIBLE
        }
    }

    private fun clearBackStack() {
        val manager = supportFragmentManager
        if (manager != null) {
            val count = manager.backStackEntryCount
            if (count > 0) {
                for (i in 0 until count) {
                    val fragment = manager.getBackStackEntryAt(i)
                    manager.popBackStack(
                        fragment.name,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE
                    ) // FragmentManager.POP_BACK_STACK_INCLUSIVE
                }
            }
        }
    }

    fun getMemberData(): MemberData? {
        return (this.applicationContext as KOTApplication).superRepository.getMemberData()

    }




    fun moveToLogin() {

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        overridePendingTransition(0,0)
    }
    fun checkStoragePermission(permissionHandler: PermissionHandler) {
        val rationale = "Please provide Media permission to get access to photos"
        val options = Permissions.Options()
            .setRationaleDialogTitle("Info")
            .setSettingsDialogTitle("Warning")
        Permissions.check(this@BaseActivity, storagePermissions, rationale, options, permissionHandler)
    }
}

