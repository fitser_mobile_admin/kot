package com.kot.ui.interfaces


interface OnYesNoClickListener {
    fun onYesClicked()
    fun onNoClicked()
}
