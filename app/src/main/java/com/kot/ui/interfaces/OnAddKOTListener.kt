package com.kot.ui.interfaces

import com.kot.model.requestmodels.AddKotItem

interface OnAddKOTListener {
    fun onAddKOT(mAddKotItem: AddKotItem)
}