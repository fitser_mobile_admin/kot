package com.kot.ui.otpverification

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.kot.R
import com.kot.core.DEVICE_TOKEN
import com.kot.core.KOTApplication
import com.kot.databinding.ActivityOtpVerificationBinding
import com.kot.model.ErrorStatusReply
import com.kot.model.requestmodels.LoginWithMobileRequest
import com.kot.model.requestmodels.LoginWithOTPRequest
import com.kot.ui.dashboard.DashboardActivity

import com.kot.ui.signupwithphone.SignupWithPhoneActivity
import com.kot.util.AndroidUtility
import com.kot.util.Prefs
import com.kot.util.SuperActivity
import android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.google.firebase.analytics.FirebaseAnalytics


class OtpVerificationActivity:SuperActivity() {

    private lateinit var binding : ActivityOtpVerificationBinding
    private lateinit var mobileNo:String
    private lateinit var countryCode:String
    private lateinit var timer: CountDownTimer
    //private  val viewModel : OTPVarificationViewModel by lazy{ ViewModelProvider(this, viewModelProvider).get(
       // OTPVarificationViewModel :: class.java)}
   // private  val viewModelProvider: OTPVarificationViewModelProvider by lazy{ OTPVarificationViewModelProvider((this.applicationContext as KOTApplication).loginWithOTPRepository, (this.applicationContext as KOTApplication)) }
    private lateinit var firebaseAnalytics:FirebaseAnalytics
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this,R.layout.activity_otp_verification)

        intent.getStringExtra("mobileNo")?.let {
            mobileNo= it
        }
        intent.getStringExtra("countryCode")?.let {
            countryCode = it
        }

        binding.tvSubmit.setOnClickListener {
            moveToDashboard()
           // clickedContinue()
        }
        binding.appcompatTextViewResendOtp.setOnClickListener {
            binding.otpView.setText("")
            //clickedResend()

        }
        binding.appcompatTextViewChangePhone.setOnClickListener {
            movetoSignInMobileNoPage()
        }

        initView()
        setUpTimeoutForResendOtp()
        startTimeoutForResendOtp()
       // initViewModelObserver()

    }

    override fun onResume() {
        super.onResume()
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        firebaseAnalytics.setCurrentScreen(this, "OTP verification", null /* class override */)
    }

   /* private fun initViewModelObserver(){
        viewModel.mediatorLiveDataLoginWithMobileResult.observe(
            this,
            Observer { t ->
                t?.let{
                    if (it.shouldReadContent()){
                        it.readContent()
                        hideProgress()
                        binding.otpView.setText("")
                        AndroidUtility.showSnackBar(this,"OTP Successfully Generated.")
                    }
                }
            })

        viewModel.mediatorLiveDataLoginWithMobileError.observe(
            this,
            Observer { t ->
                t?.let{ if (it.shouldReadContent())
                    loginError(it.getContent()?: ErrorStatusReply(1,getString(R.string.something_went_wrong),null))
                }
            }
        )

        viewModel.mediatorLiveDataLoginWithMobileOTPResult.observe(
            this,
            Observer { t ->
                t?.let{
                    if (it.shouldReadContent()){
                        it.readContent()
                        hideProgress()
                        moveToDashboard()
                    }
                }
            })

        viewModel.mediatorLiveDataLoginWithMobileOTPError.observe(
            this,
            Observer { t ->
                t?.let{ if (it.shouldReadContent())
                    loginError(it.getContent()?: ErrorStatusReply(1,getString(R.string.something_went_wrong),null))
                }
            }
        )
    }*/

    private fun loginError(errorStatusReply: ErrorStatusReply) {
        hideProgress()
        AndroidUtility.showSnackBar(this, errorStatusReply.message)
    }
    private fun initView(){

        binding.tvSubmit.setOnClickListener {
           // clickedContinue()
            moveToDashboard()
        }
        binding.appcompatTextViewChangePhone.setOnClickListener {
            movetoSignInMobileNoPage()
        }
        binding.actionBar.ivBack.setOnClickListener {
            movetoSignInMobileNoPage()
        }
      //  showSoftKeyboard(binding.otpView)
       // binding.otpView.requestFocus()
    }

    private fun movetoSignInMobileNoPage(){
       /* val intent = Intent(this, SignupWithPhoneActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
        overridePendingTransition(0,0)*/
        onBackPressed()

    }

    private fun setUpTimeoutForResendOtp() {
        timer = object : CountDownTimer(
            120000,
            1000
        ) {
            @SuppressLint("SetTextI18n")
            override fun onFinish() {
                binding.textViewTimeLeftValue.text = "(0 sec)"
                binding.appcompatTextViewResendOtp.setTextColor(resources.getColor(R.color.colorDarkYellow))
                binding.appcompatTextViewResendOtp.isClickable = true
                binding.appcompatTextViewResendOtp.isEnabled = true
            }

            @SuppressLint("SetTextI18n")
            override fun onTick(millisUntilFinished: Long) {
                binding.textViewTimeLeftValue.text = "(${millisUntilFinished / 1000} sec)"


            }
        }
    }

    fun clickedResend() {
        showProgress()
        val loginwithMobileRequest = LoginWithMobileRequest()
        loginwithMobileRequest.country_code = countryCode
        loginwithMobileRequest.mobile_no = mobileNo
        loginwithMobileRequest.device_token= Prefs.with(this).read(
            DEVICE_TOKEN,"")
        loginwithMobileRequest.device_type="2"
        //viewModel.loginWithMobile(loginwithMobileRequest)
    }

    private fun startTimeoutForResendOtp() {
        if (this::timer.isInitialized) {
            binding.appcompatTextViewResendOtp.setTextColor(resources.getColor(R.color.colorDarkBrown))
            binding.appcompatTextViewResendOtp.isClickable = false
            binding.appcompatTextViewResendOtp.isEnabled = false
            timer.start()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        timer.cancel()
        overridePendingTransition(0,0)
    }

    private fun moveToDashboard(){
        val type = intent.getStringExtra("type")

        if(type == "changePass"){
            setResult(Activity.RESULT_OK)
            onBackPressed()
        }else{
            val intent = Intent(this, DashboardActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }
    }

    fun clickedContinue() {
        if(!AndroidUtility.isNetworkAvailable(this)){
            AndroidUtility.showSnackBar(this,getString(R.string.pls_check_internet_con))
            return
        }
        binding.otpView.text?.let {
            if (
                it.isEmpty()
            ) {
                AndroidUtility.showSnackBar(
                    this,
                    "Please provide valid OTP"
                )
                return
            }
        } ?: run {
            AndroidUtility.showSnackBar(
                this,
                "Please provide valid OTP"
            )
            return
        }

        val otp = binding.otpView.text.toString()
        when {

            otp == "" -> {
                AndroidUtility.showSnackBar(this, "Email can't be left blank.")
                return
            }
        }

        showProgress()
        val loginWithOTPRequest=LoginWithOTPRequest()
        loginWithOTPRequest.countryCode = countryCode
        loginWithOTPRequest.phoneNo = mobileNo
        loginWithOTPRequest.device_token=Prefs.with(this).read(
            DEVICE_TOKEN,"")
        loginWithOTPRequest.otp = otp
        loginWithOTPRequest.device_type="2"

     //   viewModel.loginWithOTPMobile(loginWithOTPRequest)

    }


}