package com.kot.ui.otpverification

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MediatorLiveData
import com.kot.core.KOTApplication
import com.kot.model.ErrorStatusReply
import com.kot.model.Event
import com.kot.model.requestmodels.*
import com.kot.model.responsemodels.MemberData
import com.kot.model.responsemodels.SuccessStatusReply
import com.kot.repository.LoginWithOTPRepository



class OTPVarificationViewModel(private val repository: LoginWithOTPRepository, val application: KOTApplication) : AndroidViewModel(application) {

     val mediatorLiveDataLoginWithMobileOTPResult : MediatorLiveData<Event<MemberData>> = MediatorLiveData()
     val mediatorLiveDataLoginWithMobileOTPError : MediatorLiveData<Event<ErrorStatusReply>> = MediatorLiveData()


    val mediatorLiveDataLoginWithMobileResult : MediatorLiveData<Event<SuccessStatusReply>> = MediatorLiveData()
    val mediatorLiveDataLoginWithMobileError : MediatorLiveData<Event<ErrorStatusReply>> = MediatorLiveData()

    val mediatorLiveDataOTPGenerationResult : MediatorLiveData<Event<String>> = MediatorLiveData()
    val mediatorLiveDataOTPGenerationError : MediatorLiveData<Event<ErrorStatusReply>> = MediatorLiveData()

    init{
        bindToRepo()
    }

    private fun bindToRepo(){
        mediatorLiveDataLoginWithMobileOTPResult.addSource(repository.mutableLiveDataLoginOTPResult) {t -> mediatorLiveDataLoginWithMobileOTPResult.postValue(t)}
        mediatorLiveDataLoginWithMobileOTPError.addSource(repository.mutableLiveDataLoginOTPError) {t -> mediatorLiveDataLoginWithMobileOTPError.postValue(t)}

        mediatorLiveDataLoginWithMobileResult.addSource(repository.mutableLiveDataLoginResult) {t -> mediatorLiveDataLoginWithMobileResult.postValue(t)}
        mediatorLiveDataLoginWithMobileError.addSource(repository.mutableLiveDataLoginError) {t -> mediatorLiveDataLoginWithMobileError.postValue(t)}

        mediatorLiveDataOTPGenerationResult.addSource(repository.mutableLiveDataOTPResult) {t -> mediatorLiveDataOTPGenerationResult.postValue(t)}
        mediatorLiveDataOTPGenerationError.addSource(repository.mutableLiveDataOTPError) {t -> mediatorLiveDataOTPGenerationError.postValue(t)}
    }

    fun loginWithOTPMobile(loginWithOTPRequest: LoginWithOTPRequest){
        repository.loginWithOTPMobile(loginWithOTPRequest)
    }

    fun loginWithMobile(loginwithMobileRequest: LoginWithMobileRequest){
        repository.loginWithMobile(loginwithMobileRequest)
    }

    fun otpGeneration(LoginWithMobileRequest: LoginWithMobileRequest){
        repository.OTPGeneration(LoginWithMobileRequest)
    }
}