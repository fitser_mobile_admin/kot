package com.kot.ui.otpverification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kot.core.KOTApplication
import com.kot.repository.LoginWithOTPRepository

class OTPVarificationViewModelProvider(private val repository : LoginWithOTPRepository, private val application: KOTApplication) : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return OTPVarificationViewModel(repository,application) as T
    }

}