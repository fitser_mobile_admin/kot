package com.kot.ui.signupwithphone

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kot.core.KOTApplication
import com.kot.repository.LoginWithMobileRepository

class SignupMobileViewModelProvider(private val repository : LoginWithMobileRepository, private val application: KOTApplication) : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return SignUpMobileViewModel(repository,application) as T
    }

}