package com.kot.ui.signupwithphone

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.kot.R
import com.kot.core.DEVICE_TOKEN
import com.kot.core.KOTApplication
import com.kot.databinding.ActivitySignupWithPhoneBinding
import com.kot.model.ErrorStatusReply
import com.kot.model.requestmodels.LoginWithMobileRequest
import com.kot.ui.otpverification.OtpVerificationActivity
import com.kot.util.AndroidUtility
import com.kot.util.DialogUtil
import com.kot.util.Prefs
import com.kot.util.SuperActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult

class SignupWithPhoneActivity:SuperActivity() {

    private lateinit var binding : ActivitySignupWithPhoneBinding
//    private  val viewModel : SignUpMobileViewModel by lazy{ ViewModelProvider(this, viewModelProvider).get(
//        SignUpMobileViewModel :: class.java)}
//    private  val viewModelProvider: SignupMobileViewModelProvider by lazy{ SignupMobileViewModelProvider((this.applicationContext as KOTApplication).loginWithMobileRepository, (this.applicationContext as KOTApplication)) }
    private lateinit var firebaseAnalytics:FirebaseAnalytics
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup_with_phone)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_signup_with_phone)
        binding.context = this
        binding.countryCodePicker.registerCarrierNumberEditText(binding.edtPhoneNumber)

        binding.textContinue.setOnClickListener {
            //generateDeviceToken()
            //moveToNextScreen()
            moveToOTPPage("","")
        }
        binding.actionBar.ivBack.setOnClickListener {
            onBackPressed()
        }
        initViewModelObserver()

    }


    private fun generateDeviceToken(){
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(object : OnCompleteListener<InstanceIdResult> {
                override fun onComplete(task: Task<InstanceIdResult>) {
                    if (!task.isSuccessful) {
                        return
                    }
                    val token = task.result?.token?:""
                    Log.v("TOKEN:- ",token)
                    Prefs.with(this@SignupWithPhoneActivity).write(DEVICE_TOKEN,token)
                    callLoginApi()
                }
            })
    }

    private fun initViewModelObserver(){
       /* viewModel.mediatorLiveDataLoginWithMobileResult.observe(
            this,
            Observer { t ->
                t?.let{
                    if (it.shouldReadContent()){
                        it.readContent()
                        hideProgress()
                        moveToNextScreen()
                    }
                }
            })

        viewModel.mediatorLiveDataLoginWithMobileError.observe(
            this,
            Observer { t ->
                t?.let{ if (it.shouldReadContent())
                    loginError(it.getContent()?: ErrorStatusReply(1,getString(R.string.something_went_wrong),null))
                }
            }
        )*/
    }

    private fun moveToNextScreen(){
        showMessageWithOneButton(
            message = "OTP Successfully Generated.",
            cancellable = false,
            buttonText = "Okay",
            callback = object :DialogUtil.CallBack{
                override fun dialogCancelled() {

                }

                override fun buttonClicked() {
                    val mobileNo = binding.edtPhoneNumber.text.toString().trim()
                    val phoneNumberCountyCode = binding.countryCodePicker.selectedCountryCode
                    moveToOTPPage(mobileNo,phoneNumberCountyCode)
                }

            }
        )

    }

    private fun moveToOTPPage(mobileNo:String,countryCode:String){
        val intent = Intent(this, OtpVerificationActivity::class.java)
        intent.putExtra("mobileNo",mobileNo)
        intent.putExtra("countryCode",countryCode)
        intent.putExtra("type","login")
        startActivity(intent)
        overridePendingTransition(0,0)
    }

    private fun callLoginApi(){
        if(!AndroidUtility.isNetworkAvailable(this)){
            AndroidUtility.showSnackBar(this,getString(R.string.pls_check_internet_con))
            return
        }
        val mobileNo = binding.edtPhoneNumber.text.toString().trim()
        val phoneNumberCountyCode = binding.countryCodePicker.selectedCountryCode

        when{
            (!binding.countryCodePicker.isValidFullNumber) -> {
                AndroidUtility.showSnackBar(
                    this,
                    "Please enter valid phone number"
                )
                return
            }
        }
        showProgress()
        val loginwithMobileRequest = LoginWithMobileRequest()
        loginwithMobileRequest.country_code = phoneNumberCountyCode
        loginwithMobileRequest.mobile_no = mobileNo
        loginwithMobileRequest.device_token= Prefs.with(this).read(
            DEVICE_TOKEN,"")
        loginwithMobileRequest.device_type="2"
        //viewModel.loginWithMobile(loginwithMobileRequest)
        //moveToNextScreen()
    }

    private fun loginError(errorStatusReply: ErrorStatusReply) {
        hideProgress()
        AndroidUtility.showSnackBar(this, errorStatusReply.message)
    }
    override fun onResume() {
        super.onResume()

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        firebaseAnalytics.setCurrentScreen(this, "Login with phone number", null /* class override */)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        overridePendingTransition(0,0)
    }

}