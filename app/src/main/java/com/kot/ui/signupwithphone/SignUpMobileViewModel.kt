package com.kot.ui.signupwithphone

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MediatorLiveData
import com.kot.core.KOTApplication
import com.kot.model.ErrorStatusReply
import com.kot.model.Event
import com.kot.model.requestmodels.LoginWithMobileRequest
import com.kot.model.responsemodels.SuccessStatusReply
import com.kot.repository.LoginWithMobileRepository

class SignUpMobileViewModel(private val repository: LoginWithMobileRepository, val application: KOTApplication) : AndroidViewModel(application) {

     val mediatorLiveDataLoginWithMobileResult : MediatorLiveData<Event<SuccessStatusReply>> = MediatorLiveData()
     val mediatorLiveDataLoginWithMobileError : MediatorLiveData<Event<ErrorStatusReply>> = MediatorLiveData()


    init{
        bindToRepo()
    }

    private fun bindToRepo(){
        mediatorLiveDataLoginWithMobileResult.addSource(repository.mutableLiveDataLoginResult) {t -> mediatorLiveDataLoginWithMobileResult.postValue(t)}
        mediatorLiveDataLoginWithMobileError.addSource(repository.mutableLiveDataLoginError) {t -> mediatorLiveDataLoginWithMobileError.postValue(t)}
    }

    fun loginWithMobile(loginwithMobileRequest: LoginWithMobileRequest){
        repository.loginWithMobile(loginwithMobileRequest)
    }


}