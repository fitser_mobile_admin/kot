package com.kot.ui.splash

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.kot.core.KOTApplication
import com.kot.model.ErrorStatusReply
import com.kot.repository.RepositorySplash
import com.kot.model.Event
import com.kot.model.responsemodels.MemberData
import com.kot.model.responsemodels.UpdateAppResponse


class ViewModelSplash(private val repositorySplash: RepositorySplash, val application: KOTApplication) : AndroidViewModel(application) {

    var mediatorLiveDataUpdateResponse: MediatorLiveData<Event<UpdateAppResponse>> =
        MediatorLiveData()
    var mediatorLiveDataUpdateResponseError: MediatorLiveData<Event<ErrorStatusReply>> =
        MediatorLiveData()

    init {
        mediatorLiveDataUpdateResponse.addSource(
            repositorySplash.mutableLiveDataUpdateResponse
        ) { t -> mediatorLiveDataUpdateResponse.postValue(t) }
        mediatorLiveDataUpdateResponseError.addSource(
            repositorySplash.mutableLiveDataUpdateResponseError
        ) { t -> mediatorLiveDataUpdateResponseError.postValue(t) }
    }

    fun getRepo(): RepositorySplash = repositorySplash

    fun getUserData(): MemberData? {
        return repositorySplash.getMemberData()
    }


    fun clearAllData() {
        repositorySplash.clearAllData()
    }

    fun checkForUpdate() {
        repositorySplash.checkForUpdate()
    }
}