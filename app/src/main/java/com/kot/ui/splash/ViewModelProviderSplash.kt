package com.kot.ui.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kot.core.KOTApplication
import com.kot.repository.RepositorySplash

class ViewModelProviderSplash(private val repositorySplash: RepositorySplash, private val application: KOTApplication) :
    ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ViewModelSplash(repositorySplash,application) as T
    }
}