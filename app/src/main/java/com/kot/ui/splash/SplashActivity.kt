package com.kot.ui.splash

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.kot.R
import com.kot.core.KOTApplication
import com.kot.core.MEMBER_DATA
import com.kot.enum.Severity
import com.kot.enum.UpdateRequired
import com.kot.model.ErrorStatusReply
import com.kot.model.responsemodels.UpdateAppResponse
import com.kot.ui.dashboard.DashboardActivity
import com.kot.ui.login.LoginActivity
import com.kot.model.Event
import com.kot.ui.login.LoginWithEmailViewModelProvider
import com.kot.util.NetworkUtil
import com.kot.util.Prefs
import com.kot.util.SuperActivity

class SplashActivity : SuperActivity() {

    //private lateinit var viewModel: ViewModelSplash
    private var screenTimedOut = false
    private var updateChecked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        /*val viewModelProviderSplash =
            ViewModelProviderSplash((this.applicationContext as KOTApplication).splashRepository, (this.applicationContext as KOTApplication))
        viewModel = ViewModelProvider(
            this,
            viewModelProviderSplash
        )
            .get(ViewModelSplash::class.java)*/

         Handler(Looper.getMainLooper()).postDelayed({
             screenTimedOut = true
             //decideWhereToRedirect()
             moveToLogin()
         }, 3000)

         //bindToViewModel()

         /*if (NetworkUtil.isConnected(this))
            // viewModel.checkForUpdate()
         else {
             updateChecked = true
             moveToLogin()
            // decideWhereToRedirect()
         }*/
      /*  Handler().postDelayed(
            Runnable {
                moveToScreen()
            }, 3000
        )*/
    }

    private fun moveToLogin() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun decideWhereToRedirect() {
        if (!screenTimedOut || !updateChecked)
            return

        moveToDashboard()
    }

    private fun moveToDashboard() {
        val intent = Intent(this, DashboardActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun moveToScreen() {
        if (Prefs.with(this).read(MEMBER_DATA, "") != "") {
            moveToDashboard()
        } else {
            moveToLogin()
        }
    }

    /*private fun showUpdateAppMessage(updateAppResponse: UpdateAppResponse) {
        if (updateAppResponse.updateRequired == UpdateRequired.NO) {
            updateChecked = true
            decideWhereToRedirect()
            return
        }
        when (updateAppResponse.severity) {
            Severity.CRITICAL -> {
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Update Available")
                builder.setMessage(updateAppResponse.dialog_message)
                builder.setPositiveButton("Update Now") { dialogInterface, which ->
                    showsInPlayStore()
                }
                val alertDialog: AlertDialog = builder.create()
                alertDialog.setCancelable(false)
                alertDialog.show()
            }

            Severity.NON_CRITICAL -> {
                if (viewModel.getRepo().shouldShowNonCriticalUpdateMessage()) {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Update Available")
                    builder.setMessage(updateAppResponse.dialog_message)
                    builder.setPositiveButton("Install") { dialogInterface, which ->
                        showsInPlayStore()
                    }
                    //performing negative action
                    builder.setNegativeButton("Ignore") { dialogInterface, which ->
                        updateChecked = true
                        decideWhereToRedirect()
                    }
                    val alertDialog: AlertDialog = builder.create()
                    alertDialog.setCancelable(false)
                    alertDialog.show()

                } else {
                    updateChecked = true
                    decideWhereToRedirect()
                }
            }
        }
    }*/

    private fun showsInPlayStore() {
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$packageName")
                )
            )
        } catch (exception: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                )
            )
        }

        finish()
    }

    /*private fun bindToViewModel() {
        viewModel.mediatorLiveDataUpdateResponse.observe(
            this,
            Observer<Event<UpdateAppResponse>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        showUpdateAppMessage(it.getContent()!!)
                    }
                }
            }
        )
        viewModel.mediatorLiveDataUpdateResponseError.observe(
            this,
            Observer<Event<ErrorStatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        updateChecked = true
                        decideWhereToRedirect()
                    }
                }
            }
        )
    }*/
}