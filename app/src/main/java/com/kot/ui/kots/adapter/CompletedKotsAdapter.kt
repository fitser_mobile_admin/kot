package com.kot.ui.kots.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kot.databinding.AdapterCompletedKotsBinding
import com.kot.ui.interfaces.OnItemClickListener

class CompletedKotsAdapter(val mContext: Context, onItemClickListener: OnItemClickListener): RecyclerView.Adapter<CompletedKotsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    return   ViewHolder(AdapterCompletedKotsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
     return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }
    inner class ViewHolder(val binding: AdapterCompletedKotsBinding) :
        RecyclerView.ViewHolder(binding.root){
        fun onBind(){
            binding.apply {

            }

        }
    }



}