package com.kot.ui.kots

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.kot.R
import com.kot.databinding.FragmentKotsBinding
import com.kot.ui.base.BaseFragment
import com.kot.ui.interfaces.OnItemClickListener
import com.kot.ui.kots.adapter.CompletedKotsAdapter

class CompletedKotsFragment:BaseFragment("Completed KOTs") {
    private lateinit var binding: FragmentKotsBinding
    private var mCompletedKotsAdapter: CompletedKotsAdapter?=null
    companion object{
        fun newInstance(): CompletedKotsFragment {
            val mCompletedKotsFragment= CompletedKotsFragment()
            return mCompletedKotsFragment
        }
    }

    override fun getLayout(container: ViewGroup, inflater: LayoutInflater): View {
        binding= DataBindingUtil.inflate(inflater, R.layout.fragment_kots, container, false)
        return binding.root
    }

    override fun afterActivityCreated(savedInstanceState: Bundle?) {
        setAdapter()
    }

    override fun afterFragmentResume() {

    }
    private fun setAdapter(){
        mCompletedKotsAdapter = CompletedKotsAdapter(base_activity,object: OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                when(view.id){

                }
            }
        })
        val layoutManager = binding.rvKots.layoutManager as LinearLayoutManager
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.rvKots.adapter = mCompletedKotsAdapter
    }
}
