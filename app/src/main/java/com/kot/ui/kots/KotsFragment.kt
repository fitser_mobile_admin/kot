package com.kot.ui.kots

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.kot.R
import com.kot.databinding.FragmentKotsBinding
import com.kot.ui.base.BaseFragment
import com.kot.ui.interfaces.OnItemClickListener
import com.kot.ui.kots.adapter.KotsAdapter

class KotsFragment:BaseFragment("KOTs") {
    private lateinit var binding: FragmentKotsBinding
    private lateinit var mKotsAdapter: KotsAdapter
    companion object{
        fun newInstance(): KotsFragment {
            val mKotsFragment= KotsFragment()
            return mKotsFragment
        }
    }

    override fun getLayout(container: ViewGroup, inflater: LayoutInflater): View {
        binding= DataBindingUtil.inflate(inflater, R.layout.fragment_kots, container, false)
        return binding.root
    }

    override fun afterActivityCreated(savedInstanceState: Bundle?) {
        initView()
    }

    override fun afterFragmentResume() {

    }

    private fun initView(){
        setAdapter()
    }

    private fun setAdapter(){
        mKotsAdapter = KotsAdapter(base_activity,object: OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                when(view.id){

                }
            }
        })
        val layoutManager = binding.rvKots.layoutManager as LinearLayoutManager
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.rvKots.adapter = mKotsAdapter
    }
}