package com.kot.ui.kots.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kot.databinding.AdapterKotsBinding
import com.kot.ui.interfaces.OnItemClickListener

class KotsAdapter(val mContext: Context,  onItemClickListener: OnItemClickListener): RecyclerView.Adapter<KotsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    return   ViewHolder(AdapterKotsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
     return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }
    inner class ViewHolder(val binding: AdapterKotsBinding) :
        RecyclerView.ViewHolder(binding.root){
        fun onBind(){
            binding.apply {
                tvAccept.setOnClickListener {
                    tvAccept.isSelected = true
                    tvDelivered.isSelected = false
                    tvReady.isSelected = false
                }
                tvReady.setOnClickListener {
                    tvAccept.isSelected = true
                    tvDelivered.isSelected = false
                    tvReady.isSelected = true
                }
                tvDelivered.setOnClickListener {
                    tvAccept.isSelected = true
                    tvDelivered.isSelected = true
                    tvReady.isSelected = true
                }
            }

        }
    }



}