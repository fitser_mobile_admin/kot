package com.kot.ui.dashboard

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.kot.R
import com.kot.databinding.ActivityDashboardBinding
import com.kot.model.requestmodels.AddKotItem
import com.kot.ui.base.BaseActivity
import com.kot.ui.dialog.AddKOTDialog
import com.kot.ui.interfaces.OnAddKOTListener
import com.kot.ui.kots.CompletedKotsFragment
import com.kot.ui.kots.KotsFragment
class DashboardActivity : BaseActivity() {
    override val menuId: Int
        get() = R.id.iv_menu
    override val backBtnId: Int
        get() = R.id.iv_back
    override val tvTitleId: Int
        get() = R.id.tv_title
    private lateinit var binding: ActivityDashboardBinding
    private var viewItemId = R.id.navigation_kots
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard)
        setToolBar()
    }

    override fun initDrawerMenuView() {
        binding.bottomNavigationView.setOnNavigationItemSelectedListener(
            mOnNavigationItemSelectedListener)
        startFragment(BaseActivity.container, KotsFragment.newInstance(),true)
    }

    override fun onBackPressed() {
       // super.onBackPressed()
        doBack()
    }
    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener{
            when(it.itemId){
                R.id.navigation_kots -> {
                    if (viewItemId != it.itemId) {
                        viewItemId = it.itemId
                        startFragment(container, KotsFragment.newInstance(),true)
                    }

                }
                R.id.navigation_add -> {
                    if (viewItemId != it.itemId) {
                        viewItemId = it.itemId
                        showKotDialog()
                    }
                }
                R.id.navigation_completed -> {
                    if (viewItemId != it.itemId) {
                        viewItemId = it.itemId
                        startFragment(container, CompletedKotsFragment.newInstance(),true)
                    }
                }
                else->{}
            }
            true
        }

    private fun showKotDialog(){
        var mAddKOTDialog = AddKOTDialog(this, object: OnAddKOTListener{
            override fun onAddKOT(mAddKotItem: AddKotItem) {

            }
        })
        mAddKOTDialog.show()
    }

}