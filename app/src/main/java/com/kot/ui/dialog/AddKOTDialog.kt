package com.kot.ui.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.Window
import android.widget.ListView
import android.widget.PopupWindow
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import com.kot.R
import com.kot.model.requestmodels.AddKotItem
import com.kot.ui.interfaces.OnAddKOTListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AddKOTDialog(val mContext:Context, val mOnAddKOTListener: OnAddKOTListener):Dialog(mContext) {
    private lateinit var mtvAdd:AppCompatTextView
    private lateinit var edtItemName:AppCompatEditText
    private lateinit var edtQuantity:AppCompatEditText
    private val mList = ArrayList<String>()
    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_add_kots)
        val displayMetrics = DisplayMetrics()
        window?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.setLayout((width/1.3).toInt(), (width/1.3).toInt())
        setCancelable(false)
        initView()
    }

    private fun initView() {
        mList.add("Chicken burger")
        mList.add("Chicken Chilli")
        mList.add("Chicken Bharta")
        mList.add("Chicken Korma")
        mList.add("Chicken Handi")
        mtvAdd = findViewById(R.id.tvAdd)
        edtItemName = findViewById(R.id.edtItemName)
        edtItemName.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun afterTextChanged(editable: Editable?) {
                CoroutineScope(Dispatchers.Main).launch {
                    showPopUpList(editable.toString())
                }
            }

        })

        edtQuantity = findViewById(R.id.edtQuantity)
        mtvAdd.setOnClickListener {
            setData()
        }
    }
    private fun setData(){
        val mAddKotItem = AddKotItem()
        mAddKotItem.item = edtItemName.text.toString().trim()
        mAddKotItem.quantity = edtQuantity.text.toString().trim()
        dismiss()
        mOnAddKOTListener.onAddKOT(mAddKotItem)
    }



    private fun showPopUpList(mText:String){
        val filterList = mList.filter { it.contains(mText,true)}
        val inflater = LayoutInflater.from(mContext)
        val customView = inflater.inflate(R.layout.pop_list, null)
        val mListView= customView.findViewById(R.id.lvPopList) as ListView
        val mPopupWindow = PopupWindow(customView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val mPopUpItemAdapter = PopUpItemAdapter(mContext,filterList)
        mListView.adapter = mPopUpItemAdapter
        mListView.setOnItemClickListener { adapterView, view, i, l ->
            mPopupWindow.dismiss()
            edtItemName.setText(filterList[i])
           // edtItemName.setSelection(edtItemName.text.toString().length)
        }
        mPopupWindow.isOutsideTouchable = true
        mPopupWindow.showAsDropDown(edtItemName)
    }
}