package com.kot.ui.dialog

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.appcompat.widget.AppCompatTextView
import com.kot.R

class PopUpItemAdapter(val mContext: Context,val mItemList: List<String>):ArrayAdapter<ArrayList<String>>(mContext,0) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        var convertView: View? = convertView

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_pop_list_item, parent, false)
        }
        // Lookup view for data population
        val tvName: AppCompatTextView = convertView?.findViewById(R.id.tvItem) as AppCompatTextView
        tvName.text = mItemList?.get(position)
        return convertView!!
    }

    override fun getCount(): Int {
        return mItemList?.size?:0
    }


}

