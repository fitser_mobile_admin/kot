package com.kot.ui.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.TextView
import com.kot.R
import com.kot.ui.interfaces.OnYesNoClickListener


/**
 * Created by Sandeep on 13-02-2019.
 */
class CustomYesNoDialog(context: Context,val title:String,
                        val message:String, val yesText:String, val noText:String,
                        private val onYesNoClickListener: OnYesNoClickListener
) : Dialog(context),
                        View.OnClickListener {
    private var dialog_no_tv: TextView? = null
    private var dialog_yes_tv: TextView? = null
    private var dialog_title_tv:TextView?=null
    private var dialog_message_tv:TextView?=null
    init{
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_log_out)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        initView()
    }

    private fun initView() {
        dialog_title_tv = findViewById(R.id.dialog_title_tv)
        dialog_no_tv = findViewById(R.id.dialog_no_tv)
        dialog_yes_tv = findViewById(R.id.dialog_yes_tv)
        dialog_message_tv = findViewById(R.id.dialog_message_tv)
        dialog_yes_tv?.setOnClickListener(this)
        dialog_no_tv?.setOnClickListener(this)

        dialog_message_tv?.text = message
        dialog_title_tv?.text = title
        dialog_no_tv?.text = noText
        dialog_yes_tv?.text = yesText
    }


    override fun onClick(v: View?) {
        if(v!=null)
       when(v.id){
           R.id.dialog_yes_tv -> {
               dismiss()
               onYesNoClickListener.onYesClicked()
           }
           R.id.dialog_no_tv -> {
               dismiss()
               onYesNoClickListener.onNoClicked()
           }
       }
    }
}