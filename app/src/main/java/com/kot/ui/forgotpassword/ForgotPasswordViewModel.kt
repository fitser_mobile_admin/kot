package com.kot.ui.forgotpassword

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MediatorLiveData
import com.kot.core.KOTApplication
import com.kot.model.ErrorStatusReply
import com.kot.model.Event
import com.kot.model.requestmodels.ForgotPasswordRequest
import com.kot.repository.ForgotPasswordRepository

class ForgotPasswordViewModel(private val repository: ForgotPasswordRepository, val application: KOTApplication) : AndroidViewModel(application) {

     val mediatorLiveDataforgotPasswordResult : MediatorLiveData<Event<ErrorStatusReply>> = MediatorLiveData()
     val mediatorLiveDataforgotPasswordError : MediatorLiveData<Event<ErrorStatusReply>> = MediatorLiveData()


    init{
        bindToRepo()
    }

    private fun bindToRepo(){
        mediatorLiveDataforgotPasswordResult.addSource(repository.mutableLiveDataforgotPasswordResult) {t -> mediatorLiveDataforgotPasswordResult.postValue(t)}
        mediatorLiveDataforgotPasswordError.addSource(repository.mutableLiveDataforgotPasswordError) {t -> mediatorLiveDataforgotPasswordError.postValue(t)}
    }

    fun forgotPassword(forgotPasswordRequest: ForgotPasswordRequest){
        repository.forgotPassword(forgotPasswordRequest)
    }


}