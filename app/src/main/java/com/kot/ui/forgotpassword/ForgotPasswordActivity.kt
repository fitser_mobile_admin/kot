package com.kot.ui.forgotpassword

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.kot.R
import com.kot.core.KOTApplication
import com.kot.databinding.ActivityForgotPasswordBinding
import com.kot.model.ErrorStatusReply
import com.kot.model.requestmodels.ForgotPasswordRequest
import com.kot.util.AndroidUtility
import com.kot.util.DialogUtil
import com.kot.util.SuperActivity

class ForgotPasswordActivity:SuperActivity() {

    private lateinit var binding : ActivityForgotPasswordBinding
//    private  val viewModel : ForgotPasswordViewModel by lazy{ ViewModelProvider(this, viewModelProvider).get(ForgotPasswordViewModel :: class.java)}
//    private  val viewModelProvider: ForgotPasswordViewModelProvider by lazy{ ForgotPasswordViewModelProvider((this.applicationContext as KOTApplication).forgotPasswordRepository, (this.applicationContext as KOTApplication)) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)
        binding.context = this
        initView()
    }

    private fun initView(){
        binding.actionBar.toolbarTitle.setText("Forgot password")
        binding.actionBar.ivBack.setOnClickListener {
            onBackPressed()
        }
        binding.tvSubmit.setOnClickListener {
           // onBackPressed()
            callForgotPasswordApi()
        }
      //  initViewModelObserver()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        overridePendingTransition(0,0)
    }

   /* private fun initViewModelObserver(){
        viewModel.mediatorLiveDataforgotPasswordResult.observe(
            this,
            Observer { t ->
                t?.let{
                    if (it.shouldReadContent()){
                        forgotPasswordError(it.getContent()?: ErrorStatusReply(1,getString(R.string.something_went_wrong),null))

                    }
                }
            })

        viewModel.mediatorLiveDataforgotPasswordError.observe(
            this,
            Observer { t ->
                t?.let{ if (it.shouldReadContent())
                    forgotPasswordError(it.getContent()?: ErrorStatusReply(1,getString(R.string.something_went_wrong),null))
                }
            }
        )
    }*/
    private fun forgotPasswordError(errorStatusReply: ErrorStatusReply) {
        hideProgress()
        showMessageWithOneButton(
            message = errorStatusReply.message,
            cancellable = false,
            buttonText = "Okay",
            callback = object : DialogUtil.CallBack{
                override fun dialogCancelled() {

                }

                override fun buttonClicked() {
                    onBackPressed()
                }

            }
        )
    }

    private fun callForgotPasswordApi(){

        if(!AndroidUtility.isNetworkAvailable(this)){
            AndroidUtility.showSnackBar(this,getString(R.string.pls_check_internet_con))
            return
        }
        val email = binding.edtEmail.text.toString().trim()

        when {
            email == "" -> {
                AndroidUtility.showSnackBar(this, "Email can't be blank")
                return
            }
            (!AndroidUtility.isValidEmail(email)) -> {
                AndroidUtility.showSnackBar(this, "Please enter a valid email.")
                return
            }
        }
        showProgress()
        val forgotPasswordRequest =ForgotPasswordRequest()
        forgotPasswordRequest.email = email
       // viewModel.forgotPassword(forgotPasswordRequest)

    }
}