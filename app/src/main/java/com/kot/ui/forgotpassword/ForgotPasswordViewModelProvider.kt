package com.kot.ui.forgotpassword

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kot.core.KOTApplication
import com.kot.repository.ForgotPasswordRepository

class ForgotPasswordViewModelProvider(private val repository : ForgotPasswordRepository, private val application: KOTApplication) : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return ForgotPasswordViewModel(repository,application) as T
    }

}