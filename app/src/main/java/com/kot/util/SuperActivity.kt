package com.kot.util

import android.Manifest
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import com.kot.model.responsemodels.MemberData
import com.kot.ui.dialog.CustomLoaderDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.kot.core.KOTApplication
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions


abstract class SuperActivity : AppCompatActivity(),ProgressCallback{

    inline fun <reified T> fromJson(json: String): T {
        return Gson().fromJson(json, object : TypeToken<T>() {}.type)
    }

    fun toJson(argument: Any) = Gson().toJson(argument)!!

    private val dialogUtil: DialogUtil = DialogUtil(this)

    fun isConnectedToNetwork() = NetworkUtil.isConnected(this)

    private val mCustomLoaderDialog:CustomLoaderDialog by lazy{CustomLoaderDialog(this@SuperActivity)}

    private val storagePermissions = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )


    override fun showProgress() {
       mCustomLoaderDialog.show()
    }

   override fun hideProgress() {
       mCustomLoaderDialog.cancel()
    }

    fun showMessageInDialog(message: String) {
        dialogUtil.showMessage(
            message = message
        )
    }

    fun showMessageWithOneButton(
        message: String,
        callback: DialogUtil.CallBack,
        cancellable: Boolean = false,
        buttonText: String = "Ok"
    ) {
        dialogUtil.showMessage(
            message = message,
            cancellable = cancellable,
            callBack = callback,
            buttonText = buttonText
        )
    }

    fun showMessageWithTwoButton(
        message: String,
        callback: DialogUtil.MultiButtonCallBack,
        cancellable: Boolean = false,
        buttonOneText: String = "Ok",
        buttonTwoText: String = "Cancel"
    ) {
        dialogUtil.showMessage(
            message = message,
            cancellable = cancellable,
            multiButtonCallBack = callback,
            buttonOneText = buttonOneText,
            buttonTwoText = buttonTwoText
        )
    }

    private fun getSharedPref(): SharedPreferences {
        return (application as KOTApplication).superRepository.getSharedPref()
    }

    fun getMemberData(): MemberData? {
        return (application as KOTApplication).superRepository.getMemberData()
    }

    fun checkStoragePermission(permissionHandler: PermissionHandler) {
        val rationale = "Please provide Media permission to get access to photos"
        val options = Permissions.Options()
            .setRationaleDialogTitle("Info")
            .setSettingsDialogTitle("Warning")
        Permissions.check(this, storagePermissions, rationale, options, permissionHandler)
    }

}