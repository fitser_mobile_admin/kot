package com.kot.util

interface ProgressCallback {

    fun showProgress()

    fun hideProgress()
}