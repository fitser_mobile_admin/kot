package com.kot.util

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.FragmentManager
import com.kot.model.*
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.collections.ArrayList

class AndroidUtility {

    companion object {

        fun Context.dpToPx(dp: Int): Int {
            return (dp * (resources.displayMetrics.densityDpi / 160))
        }

        fun Context.pxToDp(px: Int): Int {
            return (px / resources.displayMetrics.density).toInt()
        }


        fun showToast(context: Context, text: String) {
            val dialogUtil: DialogUtil = DialogUtil(context)
            dialogUtil.showMessage(
                message = text
            )
           /* val inflater = LayoutInflater.from(context)
            // Inflate the Layout
            val layout = inflater.inflate(R.layout.custom_toast, null)
            val textView = layout.findViewById(R.id.custom_toast_tv) as TextView
            // Set the Text to show in TextView
            textView.setText(text)
            val toast = Toast(context)
            toast.duration = Toast.LENGTH_LONG
            toast.view = layout
            toast.show()*/
        }


        fun showSnackBar(context: Context, message: String) {

           val dialogUtil: DialogUtil = DialogUtil(context)
             dialogUtil.showMessage(
                message = message
            )
         /*   val toast = Toast(context)
            val layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = layoutInflater.inflate(R.layout.snak_bar_item, null)
            val textView = view.findViewById<TextView>(R.id.tvSnakeBar)
            textView.text = message
            toast.view = view
            toast.setGravity(Gravity.BOTTOM or Gravity.FILL_HORIZONTAL, 0, 0)
            toast.duration = Toast.LENGTH_LONG
            toast.show()*/

        }

        fun formatDateFromString(
            inputFormat: String,
            outputFormat: String,
            inputDate: String
        ): String {
            if (inputDate == "")
                return ""
            var parsed: Date? = null
            var outputDate = ""
            val df_input = SimpleDateFormat(inputFormat)
            val df_output = SimpleDateFormat(outputFormat)

            try {
                parsed = df_input.parse(inputDate)
                outputDate = df_output.format(parsed)

            } catch (e: ParseException) {
                Log.d("CALENDER", "ParseException - dateFormat")
            }
            return outputDate
        }

        fun validatePassword(password: String): Boolean {
            val pattern: Pattern
            val matcher: Matcher
            val PASSWORD_PATTERN = "^(?=.*[A-Za-z])(?=.*[0-9])(?=.*[!@#\$&*%^&~ ]).{8,}\$";
            pattern = Pattern.compile(PASSWORD_PATTERN);
            matcher = pattern.matcher(password);
            return matcher.matches();
        }

        fun isNetworkAvailable(context: Context): Boolean {
            val connectivity =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (connectivity != null) {
                val info = connectivity.allNetworkInfo
                if (info != null) {
                    for (i in info!!.indices) {
                        Log.w("INTERNET:", i.toString())
                        if (info!![i].getState() === NetworkInfo.State.CONNECTED) {
                            Log.w("INTERNET:", "connected!")
                            return true
                        }
                    }
                }
            }
            return false
        }

        fun getImageHeight(activity: Activity): Double {
            val display = activity.windowManager.defaultDisplay
            val outMetrics = DisplayMetrics()
            display.getMetrics(outMetrics)
            val dpHeight = outMetrics.heightPixels.toFloat()
            return dpHeight * 2.35 / 3
        }

        fun showDatePicker(
            fragmentManager: FragmentManager,
            callBack: DatePickerDialog.OnDateSetListener
        ) {
            val now = Calendar.getInstance()
            val dpd = DatePickerDialog.newInstance(
                callBack,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
            )
            dpd.maxDate = now
            dpd.isThemeDark = true
            dpd.setVersion(DatePickerDialog.Version.VERSION_1)
            dpd.show(fragmentManager, "DatePickerDialog")
        }

        fun showDatePickerWithTitle(
            fragmentManager: FragmentManager,title:String,
            callBack: DatePickerDialog.OnDateSetListener
        ) {
            val now = Calendar.getInstance()
            val dpd = DatePickerDialog.newInstance(
                callBack,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
            )
            dpd.maxDate = now
            dpd.isThemeDark = true
            dpd.setTitle(title)
            dpd.setVersion(DatePickerDialog.Version.VERSION_1)
            dpd.show(fragmentManager, "DatePickerDialog")
        }

        fun showDatePickerMinDate(
            fragmentManager: FragmentManager,
            callBack: DatePickerDialog.OnDateSetListener,minDate: Calendar
        ) {
            val dpd = DatePickerDialog.newInstance(
                callBack,
                minDate.get(Calendar.YEAR),
                minDate.get(Calendar.MONTH),
                minDate.get(Calendar.DAY_OF_MONTH)
            )
            dpd.minDate = minDate
            dpd.isThemeDark = true
            dpd.setTitle("Select Date")
            dpd.setVersion(DatePickerDialog.Version.VERSION_1)
            dpd.show(fragmentManager, "DatePickerDialog")
        }

        fun showDatePickerMaxDate(
            fragmentManager: FragmentManager,
            callBack: DatePickerDialog.OnDateSetListener,minDate: Calendar
        ) {
            val dpd = DatePickerDialog.newInstance(
                callBack,
                minDate.get(Calendar.YEAR),
                minDate.get(Calendar.MONTH),
                minDate.get(Calendar.DAY_OF_MONTH)
            )

            dpd.maxDate = minDate
            dpd.show(fragmentManager, "DatePickerDialog")
        }

        fun showDatePickerMinMaxDate(
            fragmentManager: FragmentManager,
            callBack: DatePickerDialog.OnDateSetListener,minDate: Calendar,maxDate:Calendar
        ) {
            val dpd = DatePickerDialog.newInstance(
                callBack,
                minDate.get(Calendar.YEAR),
                minDate.get(Calendar.MONTH),
                minDate.get(Calendar.DAY_OF_MONTH)
            )
            dpd.minDate = minDate
            dpd.maxDate = maxDate
            dpd.show(fragmentManager, "DatePickerDialog")
        }

       /* fun showCustomDatePickerDialog(fragmentManager: FragmentManager, callBack: DatePickerDialog.OnSelectDateListener , minDate: Calendar){


        }*/
        fun showTimePicker(
            fragmentManager: FragmentManager,
            callBack: TimePickerDialog.OnTimeSetListener,minDate: Calendar,currentDate:Calendar
        ) {
            val dpd = TimePickerDialog.newInstance(
                callBack,
                currentDate.get(Calendar.HOUR_OF_DAY),
                currentDate.get(Calendar.MINUTE),
                false
            )
         /*   if (minDate.get(Calendar.DATE).equals(currentDate.get(Calendar.DATE))&&minDate.get(Calendar.MONTH).equals(currentDate.get(Calendar.MONTH)) && minDate.get(Calendar.YEAR).equals(currentDate.get(Calendar.YEAR))){
                dpd.setMinTime(minDate.get(Calendar.HOUR_OF_DAY),minDate.get(Calendar.MINUTE),minDate.get(Calendar.SECOND))
            }*/

            dpd.setTimeInterval(1,15,30)
            dpd.show(fragmentManager, "DatePickerDialog")
        }

        fun showDatePicker(
            fragmentManager: FragmentManager,
            minDate: Calendar,
            maxDate: Calendar,
            callBack: DatePickerDialog.OnDateSetListener
        ) {
            val now = Calendar.getInstance()
            val dpd = DatePickerDialog.newInstance(
                callBack,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
            )
            dpd.minDate = minDate
            dpd.maxDate = maxDate
            dpd.show(fragmentManager, "DatePickerDialog")
        }

        fun isValidEmail(target: String): Boolean {
            return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }

        fun getCalfromDate(strDate: String): Calendar {
            val sdf = SimpleDateFormat("dd-MM-yyyy")
            val date = sdf.parse(strDate)
            val cal = Calendar.getInstance()
            cal.time = date
            return cal
        }


        @SuppressLint("SimpleDateFormat")
        fun printDifference(startTime: String, endTime: String): String {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")

            val startDate = simpleDateFormat.parse(startTime)
            val endDate = simpleDateFormat.parse(endTime)
            var different = endDate.time - startDate.time

            val secondsInMilli: Long = 1000
            val minutesInMilli = secondsInMilli * 60
            val hoursInMilli = minutesInMilli * 60
            val daysInMilli = hoursInMilli * 24

            val elapsedDays = different / daysInMilli
            different %= daysInMilli

            val elapsedHours = different / hoursInMilli
            different %= hoursInMilli

            val elapsedMinutes = different / minutesInMilli
            different %= minutesInMilli

            val elapsedSeconds = different / secondsInMilli
            var strDiff = ""
            if (elapsedHours != 0L)
                strDiff = " $elapsedHours Hr., $elapsedMinutes Min., $elapsedSeconds Sec."
            else if (elapsedMinutes != 0L)
                strDiff = "$elapsedMinutes Min., $elapsedSeconds Sec."

            return strDiff
        }

        fun getPageCount(totCount: String, maxvalue: String): String {
            if (totCount.trim() == "" || maxvalue.trim() == "")
                return ""

            var pageCount = ""
            var page = 0
            val value = totCount.toInt() % maxvalue.toInt()
            page = totCount.toInt() / maxvalue.toInt()
            if (value > 0) {
                page += 1
            }

            pageCount = page.toString()
            return pageCount
        }

        fun getTimeStamp(dateString: String): String {
            val formatter = SimpleDateFormat("yyyy-MM-dd")
            val date = formatter.parse(dateString) as Date
            return date.time.toString()
        }

        fun hideKeyboardFrom(context: Context, ) {
            val view = (context as Activity).currentFocus
            if (view != null) {
                val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }

        /*fun showStatePicker(mActivity: Activity, edtState: AppCompatEditText) {
            val mList = ArrayList<String>()
            mList.add("Victoria")
            mList.add("New South Wales")
            mList.add("Queensland")
            mList.add("Tasmania")
            mList.add("South Australia")
            mList.add("Western Australia")
            val mCustomBottomSheetDialog =
                CustomBottomSheetDialog(mActivity, mList, object : OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        edtState.setText(mList[position])
                    }
                })
            mCustomBottomSheetDialog.show()

        }*/

        fun getAge(strDate: String): String {
            val dateSdf = SimpleDateFormat("dd/MM/yyyy")
            val ageDate = dateSdf.parse(strDate)
            val daySdf = SimpleDateFormat("dd")
            val day = daySdf.format(ageDate)

            val monthSdf = SimpleDateFormat("MM")
            val month = monthSdf.format(ageDate)

            val yearSdf = SimpleDateFormat("yyyy")
            val year = yearSdf.format(ageDate)

            val dob = Calendar.getInstance()
            val today = Calendar.getInstance()

            dob.set(year.toInt(), month.toInt(), day.toInt())

            var age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)

            /*if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
                age--
            }*/

            val ageInt = age

            return ageInt.toString()
        }

        fun rotateBitmap(bitmap: Bitmap, orientation: Int): Bitmap {
            val rotatedBitmap: Bitmap
            when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 ->
                    rotatedBitmap = rotateImage(bitmap, 90f)

                ExifInterface.ORIENTATION_ROTATE_180 -> {
                    rotatedBitmap = rotateImage(bitmap, 180f)
                }
                ExifInterface.ORIENTATION_ROTATE_270 -> {
                    rotatedBitmap = rotateImage(bitmap, 270f)
                }

                ExifInterface.ORIENTATION_NORMAL -> {
                    rotatedBitmap = bitmap
                }
                else -> {
                    rotatedBitmap = bitmap
                }
            }
            return rotatedBitmap

        }

        private fun rotateImage(source: Bitmap, angle: Float): Bitmap {
            val matrix = Matrix()
            matrix.postRotate(angle)
            return Bitmap.createBitmap(
                source, 0, 0, source.width, source.height,
                matrix, true
            )
        }

        fun returnGenderList(): List<Gender> {
            val genderList = listOf<Gender>(
                Gender("Male"),
                Gender("Female")
            )

            return genderList
        }

        fun getOccassionList(): List<Occassion> {
            val occassionList = listOf<Occassion>(
                Occassion("Engagement"),
                Occassion("Cocktail Party"),
                Occassion("Birthday Party"),
                Occassion("Night Out With Friends"),
                Occassion("Dinner"),
                Occassion("others"))

            return occassionList
        }

        fun getNumberOfGuest(): ArrayList<NumberOfGuest> {
            val numberOfGuest = ArrayList<NumberOfGuest>()
            for(i in 0 until 30){
                numberOfGuest.add(NumberOfGuest((i+1).toString()))
            }

            return numberOfGuest
        }

        fun getNumberOfGuest(min:Int,max:Int): ArrayList<NumberOfGuest> {
            val numberOfGuest = ArrayList<NumberOfGuest>()
            for(i in min until max+1){
                numberOfGuest.add(NumberOfGuest((i).toString()))
            }

            return numberOfGuest
        }

        fun returnNameTitleList(): List<NameTitle> {
            val titleList = listOf<NameTitle>(
                NameTitle("Mr."),
                NameTitle("Miss."),
                NameTitle("Mrs.")
            )

            return titleList
        }

        fun returnMaritalStatusList(): List<MaritalStatus> {
            val maritalStatusList = listOf<MaritalStatus>(
                MaritalStatus("Single"),
                MaritalStatus("Married")
            )

            return maritalStatusList
        }
        fun stringToDate(aDate: String?, aFormat: String?):Date? {
            if (aDate == null) return null

            val simpledateformat = SimpleDateFormat(aFormat)
            return simpledateformat.parse(aDate)
        }

        fun getYear(endyear:Int): List<YearsData> {
            val numberOfYear = ArrayList<YearsData>()
            val currYear = Calendar.getInstance().get(Calendar.YEAR)
            for(i in currYear until endyear){
                numberOfYear.add(YearsData(""+i))
            }
            return numberOfYear
        }

        fun getMonths(): List<MonthsData> {
            val numberOfYear = ArrayList<MonthsData>()
            for(i in 0 until 12){
                numberOfYear.add(MonthsData((i+1).toString()))
            }
            return numberOfYear
        }

        

    }
}