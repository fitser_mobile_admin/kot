package com.kot.util

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import com.kot.R



class ProgressUtil(private val context: Context) {
    private lateinit var dialog: Dialog

    fun show() {
        hide()
        dialog = Dialog(context, R.style.progressBarTheme)

        dialog.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        dialog.setContentView(R.layout.progress_layout)
        dialog.setCancelable(false)
        dialog.show()

        AndroidUtility.hideKeyboardFrom(context)
    }

    fun hide() {
        @Suppress("SENSELESS_COMPARISON")
        if (this::dialog.isInitialized && dialog.isShowing)
            dialog.dismiss()
    }
}