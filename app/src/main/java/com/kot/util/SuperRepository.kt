package com.kot.util

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.kot.core.MEMBER_DATA
import com.kot.core.NON_CRITICAL_UPDATE_LAST_SHOWN
import com.kot.core.TIME_DIFF_TO_SHOW_NON_CRITICAL_UPDATE
import com.kot.model.ErrorStatusReply
import com.kot.model.Event
import com.kot.model.responsemodels.MemberData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import java.io.IOException
import java.net.SocketTimeoutException
import com.kot.enum.Result
import retrofit2.HttpException
import io.reactivex.android.schedulers.AndroidSchedulers

open class SuperRepository(private val context: Context) {

    inline fun <reified T> fromJson(json: String): T {
        return Gson().fromJson(json, object : TypeToken<T>() {}.type)
    }

    fun toJson(argument: Any) = Gson().toJson(argument)!!

    fun getSharedPref(): SharedPreferences {
        return context.getSharedPreferences(
            context.packageName + "_preferences",
            Context.MODE_PRIVATE
        )
    }

    fun saveUserDataToPref(memberData: MemberData?) {
        getSharedPref()
            .edit()
            .putString(
                MEMBER_DATA,
                toJson(memberData?:MemberData())
            )
            .apply()
    }

    fun getMemberData(): MemberData? {
        if (
            getSharedPref().getString(MEMBER_DATA, "").equals("")
        )
            return null
        return fromJson<MemberData>(
            getSharedPref().getString(
                MEMBER_DATA,
                ""
            )!!
        )
    }

    fun clearAllData() {
        getSharedPref().edit().clear().apply()
    }

    //util function
    fun getErrorMessage(throwable: Throwable): String {
        Log.d("ON_ERROR" , Log.getStackTraceString(throwable))
        return if (throwable is HttpException) {
            val responseBody = throwable.response()?.errorBody()
            try {
                val jsonObject = JSONObject(responseBody?.string()?:"")
                jsonObject.getString("error")
            } catch (e: Exception) {
                e.message!!
            }
        } else (when (throwable) {
            is SocketTimeoutException -> "Timeout occurred"
            is IOException -> "network error"
            else -> throwable.message
        })!!
    }

    @Suppress("unused")
    private fun getErrorMessage(responseBody: ResponseBody): String {
        return try {
            val jsonObject = JSONObject(responseBody.string())
            jsonObject.getString("error")
        } catch (e: Exception) {
            "Something went wrong."
        }
    }

    inline fun <reified T> makeApiCall(
        observable: Observable<Response<ResponseBody>>,
        responseJsonKeyword: String = "",
        isResponseAString: Boolean = false,
        callback: SuperRepositoryCallback<T>? = null,
        successMutableLiveData: MutableLiveData<Event<T>>? = null,
        errorMutableLiveData: MutableLiveData<Event<ErrorStatusReply>>? = null
    ) {
        observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                object : Observer<Response<ResponseBody>> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(t: Response<ResponseBody>) {
                        when (t.code()) {
                            200 -> {
                                try {
                                    val jsonObject = JSONObject(t.body()!!.string())
                                    val statusReply =
                                        fromJson<ErrorStatusReply>(jsonObject.getJSONObject("status").toString())
                                    if (statusReply.isResultOk()) {
                                        val resultToSendString =
                                            if (responseJsonKeyword == "")
                                                toJson(statusReply)
                                            else if(responseJsonKeyword == "result"){
                                                (jsonObject.getJSONObject("result")
                                                        as JSONObject).toString()
                                            } else (jsonObject.getJSONObject("response")
                                                            as JSONObject).getString(responseJsonKeyword)

                                        val resultToSend =
                                            if (isResponseAString)
                                                resultToSendString as T
                                            else fromJson(
                                                resultToSendString
                                            )

                                        successMutableLiveData?.postValue(
                                            Event(
                                                resultToSend
                                            )
                                        )

                                        callback?.success(
                                            resultToSend
                                        )
                                    } else {
                                        errorMutableLiveData?.postValue(
                                            Event(
                                                statusReply
                                            )
                                        )

                                        callback?.error(statusReply)
                                    }
                                } catch (ex: Exception) {
                                    val errorReply = ErrorStatusReply(
                                        -1,
                                        "Failed to parse data",
                                        Result.FAIL
                                    )
                                    errorMutableLiveData?.postValue(
                                        Event(
                                            errorReply
                                        )
                                    )

                                    callback?.error(errorReply)
                                }
                            }
                            else -> {
                                val errorReply = ErrorStatusReply(
                                    -1,
                                    "Failed to parse data",
                                    Result.FAIL
                                )
                                errorMutableLiveData?.postValue(
                                    Event(
                                        errorReply
                                    )
                                )

                                callback?.error(errorReply)
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        val errorReply = ErrorStatusReply(
                                -1,
                        getErrorMessage(e),
                        Result.FAIL
                        )
                        errorMutableLiveData?.postValue(
                            Event(
                                errorReply
                            )
                        )

                        callback?.error(errorReply)
                    }
                }
            )
    }


    fun shouldShowNonCriticalUpdateMessage(): Boolean {
        val nonCriticalUpdateLastShownOn = getSharedPref().getLong(
            NON_CRITICAL_UPDATE_LAST_SHOWN, 0L
        )

        if (nonCriticalUpdateLastShownOn == 0L) {
            getSharedPref()
                .edit()
                .putLong(
                    NON_CRITICAL_UPDATE_LAST_SHOWN,
                    System.currentTimeMillis()
                )
                .apply()
            return true
        }

        if (
            System.currentTimeMillis() - nonCriticalUpdateLastShownOn >
            TIME_DIFF_TO_SHOW_NON_CRITICAL_UPDATE
        ) {
            getSharedPref()
                .edit()
                .putLong(
                    NON_CRITICAL_UPDATE_LAST_SHOWN,
                    System.currentTimeMillis()
                )
                .apply()
            return true
        }
        return false
    }
}