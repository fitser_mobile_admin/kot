package com.kot.util

import com.kot.model.ErrorStatusReply


interface SuperRepositoryCallback<in T> {
    fun success(result: T){}
    fun error(statusReply: ErrorStatusReply){}
}