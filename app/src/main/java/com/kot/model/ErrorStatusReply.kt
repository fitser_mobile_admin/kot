package com.kot.model

import com.kot.enum.*


data class ErrorStatusReply(
    var error_code: Int,
    var message: String = "",
    var result: Result?
){
    fun isResultOk(): Boolean {
        if (result == null)
            result = if (error_code == 0) Result.OK else Result.FAIL

        return result == Result.OK
    }
}