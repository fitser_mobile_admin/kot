package com.kot.model

import android.graphics.drawable.Drawable
import com.kot.util.DropDownItem

data class NameTitle(val nameTitle : String) : DropDownItem {

    override fun getId(): String {
        return nameTitle
    }

    override fun getTitle(): String {
        return nameTitle
    }

    override fun getItemIcon(): Drawable? {
        return null
    }
}