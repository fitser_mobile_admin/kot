package com.kot.model.requestmodels

class LoginWithOTPRequest{
        var otp:String=""
        var phoneNo:String=""
        var countryCode:String=""
        var device_token:String=""
        var device_type:String=""
}