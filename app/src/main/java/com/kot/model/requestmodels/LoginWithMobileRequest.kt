package com.kot.model.requestmodels

class LoginWithMobileRequest{
        var country_code:String=""
        var mobile_no:String=""
        var device_token:String=""
        var device_type:String=""
}