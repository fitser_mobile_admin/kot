package com.kot.model.requestmodels

data class CheckUpdateRequest(
    val version: String
)