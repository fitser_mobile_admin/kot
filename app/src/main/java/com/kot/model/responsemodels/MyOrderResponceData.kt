package com.kot.model.responsemodels

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MyOrderResponceData(

    @field:SerializedName("food_order_id")
    val food_order_id: String? = "",

    @field:SerializedName("total_amount")
    val total_amount: String? = "",

    @field:SerializedName("discount_amount")
    val discount_amount: String? = "0",

    @field:SerializedName("payable_amount")
    val payable_amount: String? = "",

    @field:SerializedName("coupon_code")
    val coupon_code: String? = "",

    @field:SerializedName("order_status")
    val order_status: String? = "",

    @field:SerializedName("food_order_status")
    val food_order_status: String? = "",

    @field:SerializedName("order_date")
    val order_date: String? = "",

    @field:SerializedName("delivery_charge")
    val delivery_charge: String? = "0",

	@field:SerializedName("address")
	val address: AddressList? =null,

    @field:SerializedName("items")
    var items: ArrayList<MyOrderItemList>? = null,

    @field:SerializedName("addons")
    var addons: ArrayList<AddOnitemList>? = null


) : Serializable