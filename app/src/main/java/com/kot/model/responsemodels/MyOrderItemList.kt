package com.kot.model.responsemodels

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MyOrderItemList(

    @field:SerializedName("food_item_id")
    val food_item_id: String? = "",

    @field:SerializedName("category_id")
    val category_id: String? = "",

    @field:SerializedName("food_isub_category_idtem_id")
    val sub_category_id: String? = "",

    @field:SerializedName("item_name")
    val item_name: String? = "",

    @field:SerializedName("description")
    val description: String? = "",

    @field:SerializedName("price")
    val price: String? = "0",

    @field:SerializedName("slug")
    val slug: String? = "",

    @field:SerializedName("quantity")
    val quantity: String? = "0",

    @field:SerializedName("remain_book")
    val remain_items: Int? = 20,

    @field:SerializedName("item_id")
    val item_id: String? = ""

): Serializable