package com.kot.model.responsemodels

import com.google.gson.annotations.SerializedName

class Status {
    @field:SerializedName("error_code")
    val error_code: String? = ""

    @field:SerializedName("message")
    val message: String? = ""
}