package com.kot.model.responsemodels


data class MemberData(
    val member_id: String = "",
    val registration_type: String = "",
    val fb_id: String = "",
    val added_form: String = "",
    val title: String = "",
    var first_name: String = "",
    var last_name: String = "",
    var country_code: String = "",
    var mobile: String = "",
    var email: String = "",
    val original_password: String = "",
    var gender: String = "",
    var marriage_status: String = "",
    var dob: String = "",
    var doa: String = "",
    val profile_img: String = "",
    val created_by: String = "",
    val login_status: String = "",
    val status: String = "",
    val accept_terms_and_condition: String = "",
    val updated_by: String = "",
    var access_token: String = "",
    val notification_id: String = "",
    var notification_allow_type: String = "",
    var profile_image: String = "",
    var membership_id:String?=null,
    var expiry_date:String?=null,
    var profile_pic_updated:String=""
)