package com.kot.model.responsemodels

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AddOnitemList(

	@field:SerializedName("food_item_addon_id")
	val food_item_addon_id: String? = "",


	@field:SerializedName("item_id")
	val item_id: String? = "",

	@field:SerializedName("is_seen")
	val is_seen: String? = "1",

	@field:SerializedName("addon_name")
	val addon_name: String? = "",

	@field:SerializedName("quantity")
	val quantity: String? = "0",

	@field:SerializedName("is_available")
	val is_available: String? = "0",

	@field:SerializedName("addon_description")
	val addon_description: String? = "",

	@field:SerializedName("addon_price")
	val addon_price: String? = "0"

): Serializable