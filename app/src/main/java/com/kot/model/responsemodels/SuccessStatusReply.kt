package com.kot.model.responsemodels
data class SuccessStatusReply(
        var status: Status?
)