package com.kot.model.responsemodels

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AddressList(

    @field:SerializedName("food_member_address_id")
    val food_member_address_id: String? = "",

    @field:SerializedName("member_id")
    val member_id: String? = "",

    @field:SerializedName("name")
    val name: String? = "",

    @field:SerializedName("country_code")
    val country_code: String? = "",

    @field:SerializedName("phone")
    val phone: String? = "",

    @field:SerializedName("pincode")
    val pincode: String? = "",

    @field:SerializedName("state")
    val state: String? = "",

    @field:SerializedName("address")
    val address: String? = "",

    @field:SerializedName("locality")
    val locality: String? = "",

    @field:SerializedName("district")
    val district: String?="",

    @field:SerializedName("is_default")
    val is_default: String? = ""

):Serializable