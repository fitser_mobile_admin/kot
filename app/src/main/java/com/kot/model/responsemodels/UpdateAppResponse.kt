package com.kot.model.responsemodels

import com.kot.enum.Severity
import com.kot.enum.UpdateRequired


data class UpdateAppResponse(
    val updateRequired: UpdateRequired,
    val severity: Severity,
    val dialog_message:String
)