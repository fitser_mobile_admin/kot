package com.kot.model

import android.graphics.drawable.Drawable
import com.kot.util.DropDownItem

data class MaritalStatus(val maritalStatus:String) : DropDownItem {

    override fun getId(): String {
        return maritalStatus
    }

    override fun getTitle(): String {
        return maritalStatus
    }

    override fun getItemIcon(): Drawable? {
        return null
    }

}
