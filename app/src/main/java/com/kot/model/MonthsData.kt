package com.kot.model

import android.graphics.drawable.Drawable
import com.kot.util.DropDownItem

data class MonthsData(val name : String) : DropDownItem {
    override fun getId(): String {
        return name
    }

    override fun getTitle(): String {
        return name
    }

    override fun getItemIcon(): Drawable? {
        return null
    }
}