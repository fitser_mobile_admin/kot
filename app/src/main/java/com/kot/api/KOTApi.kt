package com.kot.api


import com.kot.model.requestmodels.*
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiInterface {

    @POST("login")
    fun loginWithEmail(@Body loginWithEmailRequest: LoginWithEmailRequest): Observable<Response<ResponseBody>>

    @POST("mobileOtpGenerate")
    fun mobileOTPGenerate(@Body loginWithMobileRequest: LoginWithMobileRequest): Observable<Response<ResponseBody>>

    @POST("mobileOtpGenerate")
    fun OTPGenerate(@Body loginWithMobileRequest: LoginWithMobileRequest): Observable<Response<ResponseBody>>

    @POST("loginMobileNoWithOtp")
    fun mobileOTPGenerate(@Body loginwithOTPRequest: LoginWithOTPRequest): Observable<Response<ResponseBody>>


    @POST("forgotPassword")
    fun forgotPassword(@Body forgotPasswordRequest: ForgotPasswordRequest): Observable<Response<ResponseBody>>

    @POST("version_control")
    fun checkIfUpdateRequired(
        @Body checkUpdateRequest: CheckUpdateRequest
    ): Observable<Response<ResponseBody>>

    @POST("get-ordered-history")
    fun getMyOrderRequest(@Body myOrderRequest:MyOrderRequest): Observable<Response<ResponseBody>>
    /*  @POST("changeMobileNo")
      fun changePhoneNumber(@Body changePhoneNumberRequest: ChangePhoneNumberRequest): Observable<Response<ResponseBody>>

      @POST("changeEmail")
      fun changeEmailId(@Body changeEmailRequest: ChangeEmailRequest): Observable<Response<ResponseBody>>


      @POST("changeNotificationStatus")
      fun changeNotificationStatus(@Body changeNotificationStatus: ChangeNotificationStatusRequest): Observable<Response<ResponseBody>>

      @Multipart
      @POST("editProfile")
      fun updateProfile(
          @Part("member_id") member_id: RequestBody,
          @Part("title") title: RequestBody,
          @Part("first_name") first_name: RequestBody,
          @Part("last_name") last_name: RequestBody,
          @Part("country_code") country_code: RequestBody,
          @Part("mobile") mobile: RequestBody,
          @Part("email") email: RequestBody,
          @Part("gender") gender: RequestBody,
          @Part("marriage_status") marriage_status: RequestBody,
          @Part("dob") dob: RequestBody,
          @Part("doa") doa: RequestBody,
          @Part("access_token") access_token: RequestBody,
          @Part("device_type") device_type: RequestBody,
          @Part profileImage: MultipartBody.Part?
      ): Observable<Response<ResponseBody>>

      @Multipart
      @POST("viewProfile")
      fun getProfile(
          @Part("member_id") member_id: RequestBody,
          @Part("access_token") access_token: RequestBody,
          @Part("device_type") device_type: RequestBody
      ): Observable<Response<ResponseBody>>


      @Multipart
      @POST("signup_v1")
      fun SignUpNormal(
          @Part("first_name") first_name: RequestBody,
          @Part("last_name") last_name: RequestBody,
          @Part("country_code") country_code: RequestBody,
          @Part("mobile") mobile: RequestBody,
          @Part("password") password: RequestBody,
          @Part("email") email: RequestBody,
          @Part("gender") gender: RequestBody,
          @Part("dob") dob: RequestBody,
          @Part("doa") doa: RequestBody,
          @Part("marriage_status") marriage_status: RequestBody,
          @Part("fb_id") fb_id: RequestBody,
          @Part("device_type") device_type: RequestBody,
          @Part("device_token") device_token: RequestBody,
          @Part profileImage: MultipartBody.Part?
      ): Observable<Response<ResponseBody>>


      @Multipart
      @POST("signup_v1")
      fun SignUpFacebook(
          @Part("first_name") first_name: RequestBody,
          @Part("last_name") last_name: RequestBody,
          @Part("country_code") country_code: RequestBody,
          @Part("mobile") mobile: RequestBody,
          @Part("password") password: RequestBody,
          @Part("email") email: RequestBody,
          @Part("gender") gender: RequestBody,
          @Part("dob") dob: RequestBody,
          @Part("doa") doa: RequestBody,
          @Part("marriage_status") marriage_status: RequestBody,
          @Part("fb_id") fb_id: RequestBody,
          @Part("profile_image") profile_image: RequestBody,
          @Part("device_type") device_type: RequestBody,
      @Part("device_token") device_token: RequestBody
      ): Observable<Response<ResponseBody>>


      @POST("notificationList")
      fun getNotificationList(@Body mUserIdRequest:UserIdRequest): Observable<Response<ResponseBody>>

      @POST("membershipList")
      fun getMembershipList(@Body  mUserIdRequest:UserIdRequest): Observable<Response<ResponseBody>>


      @POST("aboutusPage")
      fun getAboutusPage(): Observable<Response<ResponseBody>>

      @POST("privacyPolicy")
      fun getPrivacyPolicy(): Observable<Response<ResponseBody>>

      @POST("termsCondition")
      fun getTermsCondition(): Observable<Response<ResponseBody>>

      @POST("contactinfo")
      fun getContactUs(): Observable<Response<ResponseBody>>

      @POST("feniciaSocial")
      fun getfeniciaSocial(@Body  mUserIdRequest:UserIdRequest): Observable<Response<ResponseBody>>

      @POST("membershipDetails")
      fun getMembershipDetails(@Body  mMembershipDetailsRequest:MembershipDetailsRequest): Observable<Response<ResponseBody>>

      @POST("PreferredZone")
      fun getPreferredZone(@Body mUserIdRequest:UserIdRequest): Observable<Response<ResponseBody>>

      @POST("doReservation")
      fun doReservation(@Body mReserveNowRequest:ReserveNowRequest): Observable<Response<ResponseBody>>

      @POST("checkReservation")
      fun checkReservation(@Body mCheckReservation:CheckReservationRequest): Observable<Response<ResponseBody>>

      @POST("saveQrdata")
      fun QRCodePayment(@Body qrCodePaymentRequest: QRCodePaymentRequest): Observable<Response<ResponseBody>>

      @POST("reservationList")
      fun getReservationList(@Body mUserIdRequest:UserIdRequest): Observable<Response<ResponseBody>>

      @POST("filterReservations")
      fun getFilterReservationList(@Body mFilterRequest:FilterRequest): Observable<Response<ResponseBody>>


      @POST("updateReservation")
      fun updateReservation(@Body mUpdateReservationRequest:UpdateReservationRequest): Observable<Response<ResponseBody>>


      @POST("cancelReservation")
      fun cancelReservation(@Body mCancelReservationRequest:CancelReservationRequest): Observable<Response<ResponseBody>>

      @POST("eventList")
      fun getEventList(@Body mUserIdRequest:UserIdRequest): Observable<Response<ResponseBody>>


      @POST("eventMonthList")
      fun getEventMonthList(@Body mUserIdRequest:UserIdRequest): Observable<Response<ResponseBody>>

      @POST("dashboardImages")
      fun getDashboardImages(@Body mUserIdRequest:UserIdRequest): Observable<Response<ResponseBody>>


      @POST("latestUpdatesImage")
      fun getLatestUpdates(@Body mUserIdRequest:UserIdRequest): Observable<Response<ResponseBody>>

      @POST("galleryList")
      fun getGalleryList(@Body mGalleryRequest:GalleryRequest): Observable<Response<ResponseBody>>

      @POST("allGalleryList")
      fun getAllGalleryList(@Body mGalleryRequest:AllGalleryRequest): Observable<Response<ResponseBody>>

      @POST("fbLogin")
      fun fbLogin(@Body mFbLoginRequest:FbLoginRequest): Observable<Response<ResponseBody>>

      @POST("timeSlot")
      fun getTimeSlot(@Body mTimeSlotRequest: TimeSlotRequest): Observable<Response<ResponseBody>>

      @POST("requestForPhotograph")
      fun requestForPhotography(@Body mTimeSlotRequest: RequestForPhotographyRequest): Observable<Response<ResponseBody>>

      @POST("requestForPhotographList")
      fun myRequestPhoto(@Body mTimeSlotRequest: MyRequestForPhotoRequest): Observable<Response<ResponseBody>>

      @POST("reservationCharge")
      fun requestForReservationCharge(@Body reservationCharge: ReservationChargeRequest): Observable<Response<ResponseBody>>

      @POST("pastEventGalleryList")
      fun getPostEventGalleryList(@Body eventGalleryList: EventGalleryRequest): Observable<Response<ResponseBody>>

      @POST("checkMembership")
      fun checkMembership(@Body checkMembershipOrNot: CheckMemberShipOrNotRequest): Observable<Response<ResponseBody>>

      @POST("buyMembership")
      fun doMemberShipRequest(@Body doMemberShipRequest: DoMemberShipRequest): Observable<Response<ResponseBody>>

      @POST("doInquiry")
      fun addInquiry(@Body addInquiry: AddInquiryRequest): Observable<Response<ResponseBody>>

      @POST("membershipPaymentCheck")
      fun checkDoMemberShipId(@Body checkMemberShipOrNot: CheckMemberShipOrNotRequest): Observable<Response<ResponseBody>>

      @POST("synchronizeEventWithCalendar")
      fun synchronizeEventDataRequest(@Body synchronizeEventCalander: SynchronizeEventWithCaledarRequest): Observable<Response<ResponseBody>>


      @POST("paytm_checksum")
      fun getPaytmCheckSum(
          @Body paytemCheckSumRequest: PaytmCheckSumRequest
      ): Observable<Response<ResponseBody>>

      @POST("saveComment")
      fun saveComment(
          @Body saveCommentRequest: SaveCommentRequest
      ): Observable<Response<ResponseBody>>

      @POST("get-categories")
      fun getFoodCategory(@Body getcategoryRequest:GetCategoryRequest): Observable<Response<ResponseBody>>

      @POST("get-category-items")
      fun getFoodItems(@Body getItemListRequest: GetItemListRequest): Observable<Response<ResponseBody>>

      @POST("get-category-items")
      fun getFoodFilterItems(@Body getItemListRequest:GetFilterItemListRequest): Observable<Response<ResponseBody>>

      @POST("get-search-items")
      fun searchItemList(@Body searchItemList:SearchItemListRequest): Observable<Response<ResponseBody>>



      @POST("get-address")
      fun getAddressList(@Body getAddresListRequest:GetAddressListRequest): Observable<Response<ResponseBody>>

      @POST("get-cart-items")
      fun getCartItem(@Body getCartItemRequest:GetCartItemRequest): Observable<Response<ResponseBody>>

      @POST("remove-address")
      fun removedAddressRequest(@Body removedAddressRequest:RemovedAddressRequest): Observable<Response<ResponseBody>>

      @POST("make-default-address")
      fun makeDefaultAddress(@Body makeDefalutAddress:MakeDefalutAddressRequest): Observable<Response<ResponseBody>>

      @POST("order")
      fun foodOrder(@Body foodOrderRequest:FoodOrderRequest): Observable<Response<ResponseBody>>

      @POST("add-cart-item")
      fun addCartItem(@Body addCartItemRequest: AddCartItemRequest): Observable<Response<ResponseBody>>

      @POST("apply-coupon")
      fun applyCoupon(@Body applyCouponRequest: ApplyCouponRequest): Observable<Response<ResponseBody>>

      @POST("remove-coupon")
      fun removedCoupon(@Body removedCouponRequest: RemoveCouponRequest): Observable<Response<ResponseBody>>

      @POST("add-address")
      fun addAddress(@Body addAddressRequest: AddAddressRequest): Observable<Response<ResponseBody>>


      @POST("user-checkout")
      fun checkOutRequest(@Body checkOutRequest: CheckOutRequest): Observable<Response<ResponseBody>>

      @POST("remove-cart-item")
      fun removedCartItem(@Body removedCartItemRequest: RemovedCartItemRequest): Observable<Response<ResponseBody>>*/

}