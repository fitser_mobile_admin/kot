package  com.kot.enum

import com.google.gson.annotations.SerializedName

enum class Severity {
    @SerializedName("critical")
    CRITICAL,
    @SerializedName("nonCritical")
    NON_CRITICAL
}