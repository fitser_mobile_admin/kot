package  com.kot.enum

import com.google.gson.annotations.SerializedName

enum class UpdateRequired {
    @SerializedName("yes")
    YES,
    @SerializedName("no")
    NO
}