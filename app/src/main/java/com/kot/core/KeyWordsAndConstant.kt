package com.kot.core

//const val baseUrl = "https://www.fenicialounge.in/fenicia/api/" //live
const val baseUrl = "https://devfitser.com/Lounge/api/" //dev
const val url = "https://devfitser.com/Lounge/"
const val termsConditionURl = "${url}terms-condition"

//const val termsConditionURl="https://docs.google.com/gview?embedded=true&url=https://www.fenicialounge.in/fenicia/menu/fenicia_veg_menu.pdf"
const val privancyURl = "${url}privacy-policy"
const val contactURl = "${url}contactus"
const val payuMoneyFailure = "${url}payufailure"
const val payuMoneySuccess = "${url}payusuccess"
const val menuUrl = "${url}menu/non-veg"
const val menuVegUrl = "${url}menu/veg"
const val beverageUrl = "${url}menu/beverages"
const val alcoholUrl = "${url}menu/alcohol"

const val LOG_TAG = "lounging_log"
const val REQUEST_CODE_PICK_IMAGE = 3000
const val MEMBER_DATA = "memberDataLounging"
const val PACKAGE_ID = "package_id"
const val RESERVATION_DATA = "reservation_data"
const val MEMBERSHIP_ID = "membership_id"
const val BENEFIT_LIST = "benefit_list"
const val VOUCHER_LIST = "voucher_list"
const val RESOLVE_HINT = 1230

const val FOOD_ORDER_REQUEST="FoodOrderRequest"

const val EVENT_DATA = "event_data"
const val DEVICE_TOKEN = "device_token"
const val RESERVE_NOW_REQUEST = "ReserveNowRequest"
const val MEMBER_PRICE_INFO = "MemberPriceInfo"
const val NON_CRITICAL_UPDATE_LAST_SHOWN = "nonCriticalUpdateLastShown"
const val TIME_DIFF_TO_SHOW_NON_CRITICAL_UPDATE = 1000 * 60 * 60 * 24 * 15