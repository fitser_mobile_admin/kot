package com.kot.core

import android.app.Application
import android.util.Log
import com.kot.repository.*
import com.kot.util.Prefs
import com.kot.util.SuperRepository
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.kot.api.NetworkBuilder


class KOTApplication : Application() {
    val superRepository: SuperRepository = SuperRepository(this)

   /* val loginWithEmailRepository: LoginWithEmailRepository =
        LoginWithEmailRepository(NetworkBuilder().apiInterface, this)
    val loginWithMobileRepository: LoginWithMobileRepository =
        LoginWithMobileRepository(NetworkBuilder().apiInterface, this)
    val loginWithOTPRepository: LoginWithOTPRepository =
        LoginWithOTPRepository(NetworkBuilder().apiInterface, this)
    val forgotPasswordRepository: ForgotPasswordRepository =
        ForgotPasswordRepository(NetworkBuilder().apiInterface, this)
    val splashRepository: RepositorySplash =
        RepositorySplash(NetworkBuilder().apiInterface, this)
    val foodMyOrderRepository: FoodMyOrderRepository =
        FoodMyOrderRepository(NetworkBuilder().apiInterface, this)*/
    /*val changePhoneNumberRepository: ChangePhoneNumberRepository =
        ChangePhoneNumberRepository(NetworkBuilder().apiInterface, this)
    val changeEmailRepository: ChangeEmailRepository =
        ChangeEmailRepository(NetworkBuilder().apiInterface, this)
    val notificationStatusRepository: NotificationStatusRepository =
        NotificationStatusRepository(NetworkBuilder().apiInterface, this)
    val updateProfileRepository: UpdateProfileRepository =
        UpdateProfileRepository(NetworkBuilder().apiInterface, this)
    val mNotificationListRepository: NotificationListRepository =
        NotificationListRepository(NetworkBuilder().apiInterface, this)
    val mMembershipListRepository: MembershipListRepository =
        MembershipListRepository(NetworkBuilder().apiInterface, this)
    val aboutUsRepository: AboutUsRepository =
        AboutUsRepository(NetworkBuilder().apiInterface, this)

    val termsConditionRepository: TermsConditionRepository =
        TermsConditionRepository(NetworkBuilder().apiInterface, this)

    val privacyPolicyRepository: PrivacyPolicyRepository =
        PrivacyPolicyRepository(NetworkBuilder().apiInterface, this)

    val feniciaSocialRepository: FeniciaSocialRepository =
        FeniciaSocialRepository(NetworkBuilder().apiInterface, this)

    val membershipDetailsRepository: MembershipDetailsRepository =
        MembershipDetailsRepository(NetworkBuilder().apiInterface, this)

    val reservationRepository: ReservationRepository =
        ReservationRepository(NetworkBuilder().apiInterface, this)

    val paymentRepository: PaymentRepository =
        PaymentRepository(NetworkBuilder().apiInterface, this)

    val requestForPhotographyRepository: RequestForPhotographyRepository =
        RequestForPhotographyRepository(NetworkBuilder().apiInterface, this)

    val addInquiryRepository: AddInquiryRepository =
        AddInquiryRepository(NetworkBuilder().apiInterface, this)

    val eventRepository: EventRepository = EventRepository(NetworkBuilder().apiInterface, this)

    val eventDetailsRepository: EventDetailsRepository =
        EventDetailsRepository(NetworkBuilder().apiInterface, this)

    val eventGalleryListRepository: EventGalleryListRepository =
        EventGalleryListRepository(NetworkBuilder().apiInterface, this)

    val dashboardRepository: DashboardRepository =
        DashboardRepository(NetworkBuilder().apiInterface, this)

    val galleryRepository: GalleryRepository =
        GalleryRepository(NetworkBuilder().apiInterface, this)



    val paytmRepository: RepositoryPaytm =
        RepositoryPaytm(NetworkBuilder().apiInterface, this)

    val saveCommentRepository: SaveCommentRepository =
        SaveCommentRepository(NetworkBuilder().apiInterface, this)

    val qrCodeScanRepository: QrCodeScanRepository =
        QrCodeScanRepository(NetworkBuilder().apiInterface, this)

    val foodCategoryRepository: FoodCategoryRepository =
        FoodCategoryRepository(NetworkBuilder().apiInterface, this)

    val foodItemRepository: FoodItemRepository =
        FoodItemRepository(NetworkBuilder().apiInterface, this)

    val foodAddAddressRepository: FoodAddAddressRepository =
        FoodAddAddressRepository(NetworkBuilder().apiInterface, this)

   */

    override fun onCreate() {
        FirebaseApp.initializeApp(this)
        super.onCreate()

      //  generateDeviceToken()

    /*    Bugfender.init(this, "MUUr1Z0I2greGMxuHntR5zUK015pevxj", BuildConfig.DEBUG)
        Bugfender.enableCrashReporting()
        Bugfender.enableUIEventLogging(this)
        Bugfender.enableLogcatLogging()*/

    }

    private fun generateDeviceToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(object : OnCompleteListener<InstanceIdResult> {
                override fun onComplete(task: Task<InstanceIdResult>) {
                    if (!task.isSuccessful) {
                        return
                    }
                    val token = task.result?.token ?: ""
                    Log.v("TOKEN:- ", token)
                    Prefs.with(baseContext.applicationContext).write(DEVICE_TOKEN, token)
                }
            })

    }



}